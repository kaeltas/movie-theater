package ua.pp.kaeltas.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;

import java.util.Random;

@Component
@Aspect
@Slf4j
public class LuckyWinnerAspect {

    @Pointcut("execution(* ua.pp.kaeltas..bookTicket(..)) && args(user, ticket)")
    private void bookTicket(User user, Ticket ticket) {}

    @Around("bookTicket(user, ticket)")
    private void awardForLuckyWinner(ProceedingJoinPoint proceedingJoinPoint, User user, Ticket ticket) throws Throwable {
        if (isLucky()) {
            log.info("This ticket is lucky!!! It costs 0 !!! User {}", user);
            ticket.setPrice(0);
            proceedingJoinPoint.proceed();
        } else {
            proceedingJoinPoint.proceed();
        }
    }

    private boolean isLucky() {
        return new Random().nextBoolean();
    }

}
