package ua.pp.kaeltas.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.service.StatisticsService;

@Component
@Aspect
public class DiscountAspect {

    @Autowired
    private StatisticsService statisticsService;

    @Pointcut("execution(* ua.pp.kaeltas..getDiscount(..))")
    private void getDiscount() {}

    @After(value = "getDiscount()")
    private void countTotalDiscountRequests() {
        statisticsService.incrementTotalDiscountRequestsCount();
    }

    @After(value = "getDiscount() && args(user, ..)", argNames = "user")
    private void countDiscountRequestsForUser(User user) {
        statisticsService.incrementUserDiscountRequestsCount(user);
    }

}
