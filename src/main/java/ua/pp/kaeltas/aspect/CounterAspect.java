package ua.pp.kaeltas.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.service.StatisticsService;

@Aspect
@Component
public class CounterAspect {

    @Autowired
    private StatisticsService statisticsService;

    @Pointcut("execution(ua.pp.kaeltas.domain.Event ua.pp.kaeltas..getByName(..))")
    private void getEventByName() {}

    @AfterReturning(value = "getEventByName()", returning = "event")
    private void countGetEventByNames(Event event) {
        statisticsService.incrementEventRequestsCount(event);
    }

    @Pointcut("execution(* ua.pp.kaeltas..getTicketPrice(..)) && args(event, ..)")
    private void getTicketPrice(Event event) {}

    @After(value = "getTicketPrice(event)", argNames = "event")
    private void countGetTicketPrice(Event event) {
        statisticsService.incrementEventTicketPriceRequestsCount(event);
    }

    @Pointcut("execution(* ua.pp.kaeltas..bookTicket(..)) && args(.., ticket)")
    private void bookTicket(Ticket ticket) {}

    @After(value = "bookTicket(ticket)", argNames = "ticket")
    private void countGetTicketPrice(Ticket ticket) {
        statisticsService.incrementBookTicketRequestsCount(ticket.getPlannedEvent().getEvent());
    }
}
