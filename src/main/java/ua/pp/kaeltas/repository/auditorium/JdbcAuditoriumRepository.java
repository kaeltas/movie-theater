package ua.pp.kaeltas.repository.auditorium;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.Auditorium;

@Repository
@ProfileJdbc
@Transactional
public class JdbcAuditoriumRepository implements AuditoriumRepository {
    /*
        Mappings for field names in DB
    */
    private static final String AUDITORIUM_DB_TABLE = "auditorium";
    private static final String ID_DB_FIELD = "id";
    private static final String NAME_DB_FIELD = "name";
    private static final String TOTAL_SEATS_DB_FIELD = "total_number_of_seats";

    private static final String AUDITORIUM_VIP_SEATS_DB_TABLE = "auditorium_vip_seats";
    private static final String AVS_ID_DB_FIELD = "id";
    private static final String AVS_AUDITORIUM_ID_DB_FIELD = "auditorium_id";
    private static final String AVS_VIP_SEAT_DB_FIELD = "vip_seat";


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private List<Auditorium> auditoriums;

    @PostConstruct
    private void initialInsertDataInTable() {
        for (Auditorium auditorium : auditoriums) {
            insertAuditorium(auditorium);
            insertVipSeats(auditorium);
        }
    }

    @Override
    public List<Auditorium> getAuditoriums() {
        return jdbcTemplate.query("SELECT * FROM " + AUDITORIUM_DB_TABLE,
                (rs, rowNum) -> {
                    return mapAuditoriumFromRs(rs);
                });
    }

    private Auditorium mapAuditoriumFromRs(ResultSet rs) throws SQLException {
        Auditorium auditorium = new Auditorium();
        auditorium.setId(rs.getInt(ID_DB_FIELD));
        auditorium.setName(rs.getString(NAME_DB_FIELD));
        auditorium.setTotalNumberOfSeats(rs.getInt(TOTAL_SEATS_DB_FIELD));
        auditorium.setVipSeats(getVipSeats(auditorium));
        return auditorium;
    }

    private Set<Integer> getVipSeats(Auditorium auditorium) {
        List<Integer> vipSeats = jdbcTemplate.query("SELECT " + AVS_VIP_SEAT_DB_FIELD +
                        " FROM " + AUDITORIUM_VIP_SEATS_DB_TABLE +
                        " WHERE " + AVS_AUDITORIUM_ID_DB_FIELD + "=?",
                new Object[]{auditorium.getId()},
                (rs, rowNum) -> {
                    return rs.getInt(AVS_VIP_SEAT_DB_FIELD);
                });
        return new HashSet<>(vipSeats);
    }

    @Override
    public Auditorium findAuditoriumByName(String auditoriumName) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + AUDITORIUM_DB_TABLE +
                        " WHERE " + NAME_DB_FIELD + "=?",
                new Object[]{auditoriumName},
                (rs, rowNum) -> {
                    return mapAuditoriumFromRs(rs);
                });
    }

    @Override
    public int getSeatsNumber(String auditoriumName) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + AUDITORIUM_DB_TABLE +
                        " WHERE " + NAME_DB_FIELD + "=?",
                new Object[]{auditoriumName},
                (rs, rowNum) -> {
                    return rs.getInt(TOTAL_SEATS_DB_FIELD);
                });
    }

    @Override
    public Set<Integer> getVipSeats(String auditoriumName) {
        return findAuditoriumByName(auditoriumName).getVipSeats();
    }

    @Override
    public Auditorium findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + AUDITORIUM_DB_TABLE +
                        " WHERE " + ID_DB_FIELD + "=?",
                new Object[]{id},
                (rs, rowNum) -> {
                    return mapAuditoriumFromRs(rs);
                });
    }

    private void insertAuditorium(Auditorium auditorium) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

        Map<String, Object> params = new HashMap<>();
        params.put(NAME_DB_FIELD, auditorium.getName());
        params.put(TOTAL_SEATS_DB_FIELD, auditorium.getTotalNumberOfSeats());
        Number id = simpleJdbcInsert
                .withTableName(AUDITORIUM_DB_TABLE)
                .usingGeneratedKeyColumns(ID_DB_FIELD)
                .executeAndReturnKey(params);

        auditorium.setId(id.intValue());
    }

    private void insertVipSeats(Auditorium auditorium) {
        for (Integer seat : auditorium.getVipSeats()) {
            jdbcTemplate.update("INSERT INTO " + AUDITORIUM_VIP_SEATS_DB_TABLE +
                            " (" + AVS_AUDITORIUM_ID_DB_FIELD + ", " + AVS_VIP_SEAT_DB_FIELD + ") " +
                            "VALUES" + "(?, ?)",
                    auditorium.getId(),
                    seat);

        }
    }
}
