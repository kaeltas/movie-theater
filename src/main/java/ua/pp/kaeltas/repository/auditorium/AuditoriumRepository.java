package ua.pp.kaeltas.repository.auditorium;

import java.util.List;
import java.util.Set;

import ua.pp.kaeltas.domain.Auditorium;

public interface AuditoriumRepository {

    List<Auditorium> getAuditoriums();
    Auditorium findAuditoriumByName(String auditoriumName);
    int getSeatsNumber(String auditoriumName);
    Set<Integer> getVipSeats(String auditoriumName);
    Auditorium findById(int id);
}
