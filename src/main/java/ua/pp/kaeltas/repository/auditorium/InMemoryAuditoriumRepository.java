package ua.pp.kaeltas.repository.auditorium;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ua.pp.kaeltas.config.ProfileInMemory;
import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.exception.NoSuchAuditoriumException;

@Repository
@ProfileInMemory
public class InMemoryAuditoriumRepository implements AuditoriumRepository {

    @Autowired
    private List<Auditorium> auditoriums;

    @Override
    public List<Auditorium> getAuditoriums() {
        return new LinkedList<>(auditoriums);
    }

    @Override
    public Auditorium findAuditoriumByName(String auditoriumName) {
        return findFirstAuditoriumByName(auditoriumName);
    }

    @Override
    public int getSeatsNumber(String auditoriumName) {
        return findFirstAuditoriumByName(auditoriumName)
                .getTotalNumberOfSeats();
    }

    @Override
    public Set<Integer> getVipSeats(String auditoriumName) {
        return findFirstAuditoriumByName(auditoriumName)
                .getVipSeats();
    }

    @Override
    public Auditorium findById(int id) {
        return auditoriums.stream()
                .filter(a -> id == a.getId())
                .findFirst()
                .orElseThrow(() -> new NoSuchAuditoriumException(id));
    }

    private Auditorium findFirstAuditoriumByName(String auditoriumName) {
        return auditoriums.stream()
                .filter(p -> auditoriumName.equals(p.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchAuditoriumException(auditoriumName));
    }
}
