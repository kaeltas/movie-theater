package ua.pp.kaeltas.repository.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.User;

@Repository
@ProfileJdbc
@Transactional
public class JdbcStatisticsRepository implements StatisticsRepository {
    /*
        Mappings for field names in DB
     */
    private static final String EVENT_STATISTIC_DB_TABLE = "event_statistics";
    private static final String ID_DB_FIELD = "id";
    private static final String EVENT_ID_DB_FIELD = "event_id";
    private static final String STATISTICS_TYPE_DB_FIELD = "statistic_type";
    private static final String VALUE_DB_FIELD = "value";

    private static final int EVENT_REQUESTS_COUNTER_TYPE = 1;
    private static final int TICKET_PRICE_REQUESTS_COUNTER_TYPE = 2;
    private static final int BOOK_TICKET_REQUESTS_COUNTER_TYPE = 3;

    private static final String USER_STATISTIC_DB_TABLE = "user_statistics";
    private static final String US_ID_DB_FIELD = "id";
    private static final String US_USER_ID_DB_FIELD = "user_id";
    private static final String US_VALUE_DB_FIELD = "value";

    private static final int TOTAL_DISCOUNT_REQUESTS_COUNT_ID = Integer.MAX_VALUE;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void saveEventRequestsCount(Event event, int count) {
        saveCounterForStatisticType(event, count, EVENT_REQUESTS_COUNTER_TYPE);
    }

    @Override
    @Transactional(readOnly = true)
    public int getEventRequestsCount(Event event) {
        return getCounterForStatisticType(new Object[]{event.getId(), EVENT_REQUESTS_COUNTER_TYPE});
    }

    @Override
    public void saveEventTicketPriceRequestsCount(Event event, int count) {
        saveCounterForStatisticType(event, count, TICKET_PRICE_REQUESTS_COUNTER_TYPE);
    }

    @Override
    @Transactional(readOnly = true)
    public int getEventTicketPriceRequestsCount(Event event) {
        return getCounterForStatisticType(new Object[]{event.getId(), TICKET_PRICE_REQUESTS_COUNTER_TYPE});
    }

    @Override
    public void saveBookTicketRequestsCount(Event event, int count) {
        saveCounterForStatisticType(event, count, BOOK_TICKET_REQUESTS_COUNTER_TYPE);
    }

    @Override
    @Transactional(readOnly = true)
    public int getBookTicketRequestsCount(Event event) {
        return getCounterForStatisticType(new Object[]{event.getId(), BOOK_TICKET_REQUESTS_COUNTER_TYPE});
    }

    @Override
    public void saveTotalDiscountRequestsCount(int count) {
        if (0 == getTotalDiscountRequestsCount()) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(US_USER_ID_DB_FIELD, TOTAL_DISCOUNT_REQUESTS_COUNT_ID);
            params.put(US_VALUE_DB_FIELD, count);
            Number id = simpleJdbcInsert
                    .withTableName(USER_STATISTIC_DB_TABLE)
                    .execute(params);
        } else {
            jdbcTemplate.update("UPDATE " + USER_STATISTIC_DB_TABLE + " SET " +
                            US_VALUE_DB_FIELD + "=? " +
                            "WHERE " + US_USER_ID_DB_FIELD + "=? ",
                    count,
                    TOTAL_DISCOUNT_REQUESTS_COUNT_ID);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public int getTotalDiscountRequestsCount() {
        try {
            return jdbcTemplate.queryForObject("SELECT " + US_VALUE_DB_FIELD +
                            " FROM " + USER_STATISTIC_DB_TABLE +
                            " WHERE " + US_USER_ID_DB_FIELD + "=?",
                    new Object[]{TOTAL_DISCOUNT_REQUESTS_COUNT_ID}, Integer.class);
        } catch (EmptyResultDataAccessException ex) {
            return 0;
        }
    }

    @Override
    public void saveUserDiscountRequestsCount(User user, int count) {
        if (0 == getUserDiscountRequestsCount(user)) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(US_USER_ID_DB_FIELD, user.getId());
            params.put(US_VALUE_DB_FIELD, count);
            simpleJdbcInsert
                    .withTableName(USER_STATISTIC_DB_TABLE)
                    .execute(params);
        } else {
            jdbcTemplate.update("UPDATE " + USER_STATISTIC_DB_TABLE + " SET " +
                            US_VALUE_DB_FIELD + "=? " +
                            "WHERE " + US_USER_ID_DB_FIELD + "=? ",
                    count,
                    user.getId());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public int getUserDiscountRequestsCount(User user) {
        try {
            return jdbcTemplate.queryForObject("SELECT " + US_VALUE_DB_FIELD +
                            " FROM " + USER_STATISTIC_DB_TABLE +
                            " WHERE " + US_USER_ID_DB_FIELD + "=?",
                    new Object[]{user.getId()}, Integer.class);
        } catch (EmptyResultDataAccessException ex) {
            return 0;
        }
    }


    private void saveCounterForStatisticType(Event event, int count, int requestsCounterType) {
        if (0 == getCounterForStatisticType(new Object[]{event.getId(), requestsCounterType})) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(EVENT_ID_DB_FIELD, event.getId());
            params.put(STATISTICS_TYPE_DB_FIELD, requestsCounterType);
            params.put(VALUE_DB_FIELD, count);
            simpleJdbcInsert
                    .withTableName(EVENT_STATISTIC_DB_TABLE)
                    .execute(params);
        } else {
            jdbcTemplate.update("UPDATE " + EVENT_STATISTIC_DB_TABLE + " SET " +
                            VALUE_DB_FIELD + "=? " +
                            "WHERE " + EVENT_ID_DB_FIELD + "=? AND " +
                            STATISTICS_TYPE_DB_FIELD + "=?",
                    count,
                    event.getId(),
                    requestsCounterType);
        }
    }

    private int getCounterForStatisticType(Object[] args) {
        try {
            return jdbcTemplate.queryForObject("SELECT " + VALUE_DB_FIELD +
                            " FROM " + EVENT_STATISTIC_DB_TABLE +
                            " WHERE " + EVENT_ID_DB_FIELD + "=? AND " +
                            STATISTICS_TYPE_DB_FIELD + "=?",
                    args, Integer.class);
        } catch (EmptyResultDataAccessException ex) {
            return 0;
        }
    }
}
