package ua.pp.kaeltas.repository.statistics;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

import ua.pp.kaeltas.config.ProfileInMemory;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.User;

@Repository
@ProfileInMemory
public class InMemoryStatisticsRepository implements StatisticsRepository {
    private Map<Event, Integer> eventsCounter = new HashMap<>();
    private Map<Event, Integer> ticketPriceRequestsCounter = new HashMap<>();
    private Map<Event, Integer> bookTicketsRequestsCounter = new HashMap<>();

    private int totalDiscountRequestsCount = 0;
    private Map<User, Integer> userDiscountRequestsCounter = new HashMap<>();

    @Override
    public void saveEventRequestsCount(Event event, int count) {
        eventsCounter.put(event, count);
    }

    @Override
    public int getEventRequestsCount(Event event) {
        return eventsCounter.get(event) == null ? 0 : eventsCounter.get(event);
    }

    @Override
    public void saveEventTicketPriceRequestsCount(Event event, int count) {
        ticketPriceRequestsCounter.put(event, count);
    }

    @Override
    public int getEventTicketPriceRequestsCount(Event event) {
        return ticketPriceRequestsCounter.get(event) == null ? 0 : ticketPriceRequestsCounter.get(event);
    }

    @Override
    public void saveBookTicketRequestsCount(Event event, int count) {
        bookTicketsRequestsCounter.put(event, count);
    }

    @Override
    public int getBookTicketRequestsCount(Event event) {
        return bookTicketsRequestsCounter.get(event) == null ? 0 : bookTicketsRequestsCounter.get(event);
    }

    @Override
    public void saveTotalDiscountRequestsCount(int count) {
        totalDiscountRequestsCount = count;
    }

    @Override
    public int getTotalDiscountRequestsCount() {
        return totalDiscountRequestsCount;
    }

    @Override
    public void saveUserDiscountRequestsCount(User user, int count) {
        userDiscountRequestsCounter.put(user, count);
    }

    @Override
    public int getUserDiscountRequestsCount(User user) {
        return userDiscountRequestsCounter.get(user) == null ? 0 : userDiscountRequestsCounter.get(user);
    }

}
