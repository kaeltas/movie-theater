package ua.pp.kaeltas.repository.statistics;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.User;

public interface StatisticsRepository {

    void saveEventRequestsCount(Event event, int count);
    int getEventRequestsCount(Event event);

    void saveEventTicketPriceRequestsCount(Event event, int count);
    int getEventTicketPriceRequestsCount(Event event);

    void saveBookTicketRequestsCount(Event event, int count);
    int getBookTicketRequestsCount(Event event);

    void saveTotalDiscountRequestsCount(int count);
    int getTotalDiscountRequestsCount();

    void saveUserDiscountRequestsCount(User user, int count);
    int getUserDiscountRequestsCount(User user);
}
