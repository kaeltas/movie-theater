package ua.pp.kaeltas.repository.event;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ua.pp.kaeltas.config.ProfileInMemory;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.exception.NoSuchEventException;
import ua.pp.kaeltas.exception.SuchEventAlreadyExistsException;

@Repository
@ProfileInMemory
public class InMemoryEventRepository implements EventRepository {

    private Map<String, Event> eventsMap = new ConcurrentHashMap<>();

    @Override
    public void save(Event event) {
        if (eventsMap.containsKey(event.getName())) {
            throw new SuchEventAlreadyExistsException(event);
        }
        eventsMap.put(event.getName(), event);
    }

    @Override
    public void remove(String name) {
        eventsMap.remove(name);
    }

    @Override
    public Event findByName(String name) {
        if (!eventsMap.containsKey(name)) {
            throw new NoSuchEventException(name);
        }
        return eventsMap.get(name);
    }

    @Override
    public List<Event> findAll() {
        return new ArrayList<>(eventsMap.values());
    }

    @Override
    public Event findById(int id) {
        return eventsMap.values().stream()
                .filter(v -> id == v.getId())
                .findFirst()
                .orElseThrow(() -> new NoSuchEventException(id));
    }
}
