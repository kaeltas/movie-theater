package ua.pp.kaeltas.repository.event;

import java.time.LocalDateTime;
import java.util.List;

import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;

public interface PlannedEventRepository {

    /**
     *  Returns events for specified date range
     */
    List<PlannedEvent> findForDateRange(LocalDateTime from, LocalDateTime to);

    void assignAuditorium(Event event, Auditorium auditorium, LocalDateTime date);

    boolean contains(String eventName);

    PlannedEvent findByEventAndDate(Event event, LocalDateTime dateTime);

    List<PlannedEvent> findAllByDate(LocalDateTime dateTime);

    List<PlannedEvent> findAllByEvent(Event event);

    PlannedEvent findById(int planned_event_id);
}
