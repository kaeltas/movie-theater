package ua.pp.kaeltas.repository.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.EventRating;

@Repository
@ProfileJdbc
@Transactional
public class JdbcEventRepository implements EventRepository {
    /*
        Mappings for field names in DB
     */
    private static final String EVENT_DB_TABLE = "event";
    private static final String ID_DB_FIELD = "id";
    private static final String NAME_DB_FIELD = "name";
    private static final String RATING_DB_FIELD = "rating";
    private static final String BASE_PRICE_DB_FIELD = "base_price";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(Event event) {
        if (null == event.getId()) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(NAME_DB_FIELD, event.getName());
            params.put(RATING_DB_FIELD, event.getRating().name());
            params.put(BASE_PRICE_DB_FIELD, event.getBasePrice());
            Number id = simpleJdbcInsert
                    .withTableName(EVENT_DB_TABLE)
                    .usingGeneratedKeyColumns(ID_DB_FIELD)
                    .executeAndReturnKey(params);

            event.setId(id.intValue());
        } else {
            jdbcTemplate.update("UPDATE " + EVENT_DB_TABLE + " SET " +
                            NAME_DB_FIELD + "=?, " +
                            RATING_DB_FIELD + "=?, " +
                            BASE_PRICE_DB_FIELD + "=? " +
                            "WHERE " + ID_DB_FIELD + "=?",
                    event.getName(),
                    event.getRating(),
                    event.getBasePrice(),
                    event.getId());
        }
    }

    @Override
    public void remove(String name) {
        jdbcTemplate.update("DELETE FROM " + EVENT_DB_TABLE + " WHERE " + NAME_DB_FIELD + "=?", name);
    }

    @Override
    @Transactional(readOnly = true)
    public Event findByName(String name) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + EVENT_DB_TABLE +
                        " WHERE " + NAME_DB_FIELD + "=?",
                new Object[]{name},
                (rs, rowNum) -> {
                    return mapEventFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> findAll() {
        return jdbcTemplate.query("SELECT * FROM " + EVENT_DB_TABLE,
                (rs, rowNum) -> {
                    return mapEventFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public Event findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + EVENT_DB_TABLE +
                        " WHERE " + ID_DB_FIELD + "=?",
                new Object[]{id},
                (rs, rowNum) -> {
                    return mapEventFromRs(rs);
                });
    }

    private Event mapEventFromRs(ResultSet rs) throws SQLException {
        Event event = new Event(rs.getString(NAME_DB_FIELD),
                rs.getDouble(BASE_PRICE_DB_FIELD),
                EventRating.valueOf(rs.getString(RATING_DB_FIELD)));
        event.setId(rs.getInt(ID_DB_FIELD));
        return event;
    }
}
