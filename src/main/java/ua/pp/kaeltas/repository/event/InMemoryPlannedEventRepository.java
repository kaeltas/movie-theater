package ua.pp.kaeltas.repository.event;

import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ua.pp.kaeltas.config.ProfileInMemory;
import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.exception.NoSuchPlannedEventException;
import ua.pp.kaeltas.exception.SuchAuditoriumAlreadyAssignedException;

@Repository
@ProfileInMemory
public class InMemoryPlannedEventRepository implements PlannedEventRepository {
    private List<PlannedEvent> plannedEvents = new ArrayList<>();

    @Override
    public List<PlannedEvent> findForDateRange(LocalDateTime from, LocalDateTime to) {
        return plannedEvents.stream()
                .filter(
                        p -> (from.isBefore(p.getDateTime()) || from.isEqual(p.getDateTime()))
                                &&
                                (to.isAfter(p.getDateTime()) || to.isEqual(p.getDateTime()))
                )
                .collect(Collectors.toList());
    }

    @Override
    public void assignAuditorium(Event event, Auditorium auditorium, LocalDateTime date) {
        PlannedEvent plannedEvent = new PlannedEvent(event, auditorium, date);
        if (plannedEvents.stream().anyMatch(p -> auditorium.equals(p.getAuditorium()) && date.equals(p.getDateTime()))) {
            throw new SuchAuditoriumAlreadyAssignedException(auditorium, date);
        }
        plannedEvents.add(plannedEvent);
    }

    @Override
    public boolean contains(String eventName) {
        return plannedEvents.stream()
                .anyMatch(p -> eventName.equals(p.getEvent().getName()));
    }

    @Override
    public PlannedEvent findByEventAndDate(Event event, LocalDateTime dateTime) {
        return findFirstPlannedEventByEventAndDateTime(event, dateTime);
    }

    @Override
    public List<PlannedEvent> findAllByDate(LocalDateTime dateTime) {
        return plannedEvents.stream()
                .filter(p -> p.getDateTime().equals(dateTime))
                .collect(Collectors.toList());
    }

    @Override
    public List<PlannedEvent> findAllByEvent(Event event) {
        return plannedEvents.stream()
                .filter(p -> p.getEvent().getId().equals(event.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public PlannedEvent findById(int id) {
        return plannedEvents.stream()
                .filter(p -> id == p.getId())
                .findFirst()
                .orElseThrow(() -> new NoSuchPlannedEventException(id));
    }

    private PlannedEvent findFirstPlannedEventByEventAndDateTime(Event event, LocalDateTime dateTime) {
        return plannedEvents.stream()
                .filter(p -> event.equals(p.getEvent()) && dateTime.equals(p.getDateTime()))
                .findFirst()
                .orElseThrow(() -> new NoSuchPlannedEventException(event, dateTime));
    }
}
