package ua.pp.kaeltas.repository.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.repository.auditorium.AuditoriumRepository;

@Repository
@ProfileJdbc
@Transactional
public class JdbcPlannedEventRepository implements PlannedEventRepository {
    /*
        Mappings for field names in DB
     */
    private static final String PLANNED_EVENT_DB_TABLE = "planned_event";
    private static final String ID_DB_FIELD = "id";
    private static final String EVENT_ID_DB_FIELD = "event_id";
    private static final String AUDITORIUM_ID_DB_FIELD = "auditorium_id";
    private static final String DATETIME_DB_FIELD = "datetime";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private AuditoriumRepository auditoriumRepository;

    @Override
    public List<PlannedEvent> findForDateRange(LocalDateTime from, LocalDateTime to) {
        return null;
    }

    @Override
    public void assignAuditorium(Event event, Auditorium auditorium, LocalDateTime date) {
        if (null == event || null == event.getId()
                || null == auditorium || null == auditorium.getId()
                || null == date) {
            throw new IllegalArgumentException("assignAuditorium(..) - Args can't be null");
        }

        jdbcTemplate.update("INSERT INTO " + PLANNED_EVENT_DB_TABLE + " (" +
                        EVENT_ID_DB_FIELD + ", " +
                        AUDITORIUM_ID_DB_FIELD + ", " +
                        DATETIME_DB_FIELD + ") " +
                        "VALUES (?, ?, ?)",
                event.getId(),
                auditorium.getId(),
                Timestamp.valueOf(date)
        );
    }

    @Override
    @Transactional(readOnly = true)
    public boolean contains(String eventName) {
        Integer count = jdbcTemplate.queryForObject("SELECT count(*) FROM planned_event pe, event e " +
                " WHERE pe.event_id = e.id AND e.name = ?", new Object[]{eventName}, Integer.class);
        return count != 0;
    }

    @Override
    @Transactional(readOnly = true)
    public PlannedEvent findByEventAndDate(Event event, LocalDateTime dateTime) {
        if (null == event || null == event.getId()) {
            throw new IllegalArgumentException("findByEventAndDate(..) args can't be null" );
        }
        return jdbcTemplate.queryForObject("SELECT * FROM " + PLANNED_EVENT_DB_TABLE +
                        " WHERE " + EVENT_ID_DB_FIELD + "=? AND " + DATETIME_DB_FIELD + "=?",
                new Object[]{event.getId(), Timestamp.valueOf(dateTime)},
                (rs, rowNum) -> {
                    return mapPlannedEventFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlannedEvent> findAllByDate(LocalDateTime dateTime) {
        return jdbcTemplate.query("SELECT * FROM " + PLANNED_EVENT_DB_TABLE +
                        " WHERE " + DATETIME_DB_FIELD + "=?", new Object[]{Timestamp.valueOf(dateTime)},
                (rs, rowNum) -> {
                    return mapPlannedEventFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlannedEvent> findAllByEvent(Event event) {
        return jdbcTemplate.query("SELECT * FROM " + PLANNED_EVENT_DB_TABLE +
                        " WHERE " + EVENT_ID_DB_FIELD + "=?", new Object[]{event.getId()},
                (rs, rowNum) -> {
                    return mapPlannedEventFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public PlannedEvent findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + PLANNED_EVENT_DB_TABLE +
                        " WHERE " + ID_DB_FIELD + "=?",
                new Object[]{id},
                (rs, rowNum) -> {
                    return mapPlannedEventFromRs(rs);
                });
    }

    private PlannedEvent mapPlannedEventFromRs(ResultSet rs) throws SQLException {
        Event event = eventRepository.findById(rs.getInt(EVENT_ID_DB_FIELD));
        Auditorium auditorium = auditoriumRepository.findById(rs.getInt(AUDITORIUM_ID_DB_FIELD));

        PlannedEvent plannedEvent = new PlannedEvent(event, auditorium, rs.getTimestamp(DATETIME_DB_FIELD).toLocalDateTime());
        plannedEvent.setId(rs.getInt(ID_DB_FIELD));
        return plannedEvent;
    }
}
