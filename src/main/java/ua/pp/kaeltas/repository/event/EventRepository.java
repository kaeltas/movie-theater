package ua.pp.kaeltas.repository.event;

import java.util.List;

import ua.pp.kaeltas.domain.Event;

public interface EventRepository {

    void save(Event event);
    void remove(String name);
    Event findByName(String name);
    List<Event> findAll();

    Event findById(int id);
}
