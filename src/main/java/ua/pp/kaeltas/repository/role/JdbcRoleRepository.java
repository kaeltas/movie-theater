package ua.pp.kaeltas.repository.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.Role;

@Repository
@ProfileJdbc
@Transactional
public class JdbcRoleRepository implements RoleRepository {
    /*
        Mappings for field names in DB
     */
    private static final String ROLE_DB_TABLE = "role";
    private static final String ID_DB_FIELD = "id";
    private static final String USERNAME_DB_FIELD = "username";
    private static final String ROLE_DB_FIELD = "role";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void save(Role role) {
        if (null == role.getId()) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(USERNAME_DB_FIELD, role.getUserName());
            params.put(ROLE_DB_FIELD, role.getRoleName());
            Number id = simpleJdbcInsert
                    .withTableName(ROLE_DB_TABLE)
                    .usingGeneratedKeyColumns(ID_DB_FIELD)
                    .executeAndReturnKey(params);

            role.setId(id.intValue());
        } else {
            jdbcTemplate.update("UPDATE " + ROLE_DB_TABLE + " " +
                            "SET " +
                            USERNAME_DB_FIELD + "=?, " +
                            ROLE_DB_FIELD + "=? " +
                            "WHERE " +
                            ID_DB_FIELD + "=?",
                    role.getUserName(),
                    role.getRoleName(),
                    role.getId());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Role> findAllByUsername(String username) {
        return jdbcTemplate.query("SELECT * FROM " + ROLE_DB_TABLE + " WHERE " + USERNAME_DB_FIELD + "=?", new Object[]{username},
                (rs, rowNum) -> {
                    return mapRoleFromRs(rs);
                });
    }

    private Role mapRoleFromRs(ResultSet rs) throws SQLException {
        Role role = new Role(rs.getString(USERNAME_DB_FIELD),
                rs.getString(ROLE_DB_FIELD));
        role.setId(rs.getInt(ID_DB_FIELD));
        return role;
    }

}
