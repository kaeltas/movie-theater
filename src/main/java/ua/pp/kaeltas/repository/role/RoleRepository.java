package ua.pp.kaeltas.repository.role;

import java.util.List;

import ua.pp.kaeltas.domain.Role;

public interface RoleRepository {
    void save(Role role);
    List<Role> findAllByUsername(String username);
}
