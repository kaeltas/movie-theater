package ua.pp.kaeltas.repository.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.event.PlannedEventRepository;
import ua.pp.kaeltas.repository.user.UserRepository;

@Repository
@ProfileJdbc
@Transactional
public class JdbcTicketRepository implements TicketRepository {
    /*
        Mappings for field names in DB
     */
    private static final String TICKET_DB_TABLE = "ticket";
    private static final String ID_DB_FIELD = "id";
    private static final String USER_ID_DB_FIELD = "user_id";
    private static final String PLANNED_EVENT_ID_DB_FIELD = "planned_event_id";
    private static final String SEAT_DB_FIELD = "seat";
    private static final String PRICE_DB_FIELD = "price";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PlannedEventRepository plannedEventRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(Ticket ticket) {
        if (null == ticket.getId()) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(USER_ID_DB_FIELD, ticket.getUser().getId());
            params.put(PLANNED_EVENT_ID_DB_FIELD, ticket.getPlannedEvent().getId());
            params.put(SEAT_DB_FIELD, ticket.getSeat());
            params.put(PRICE_DB_FIELD, ticket.getPrice());
            Number id = simpleJdbcInsert
                    .withTableName(TICKET_DB_TABLE)
                    .usingGeneratedKeyColumns(ID_DB_FIELD)
                    .executeAndReturnKey(params);

            ticket.setId(id.intValue());
        } else {
            jdbcTemplate.update("UPDATE " + TICKET_DB_TABLE + " SET " +
                            USER_ID_DB_FIELD + "=?, " +
                            PLANNED_EVENT_ID_DB_FIELD + "=?, " +
                            SEAT_DB_FIELD + "=?, " +
                            PRICE_DB_FIELD + "=? " +
                            "WHERE " + ID_DB_FIELD + "=?",
                    ticket.getUser().getId(),
                    ticket.getPlannedEvent().getId(),
                    ticket.getSeat(),
                    ticket.getPrice(),
                    ticket.getId());
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> findAllByEvent(Event event, LocalDateTime dateTime) {
        PlannedEvent plannedEvent = plannedEventRepository.findByEventAndDate(event, dateTime);

        return jdbcTemplate.query("SELECT * FROM " + TICKET_DB_TABLE + " " +
                        "WHERE " + PLANNED_EVENT_ID_DB_FIELD + "=?", new Object[]{plannedEvent.getId()},
                (rs, rowNum) -> {
                    return mapTicketFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> findAllByUser(User user) {
        if (null == user || null == user.getId()) {
            return Collections.emptyList();
        }
        return jdbcTemplate.query("SELECT * FROM " + TICKET_DB_TABLE + " " +
                        "WHERE " + USER_ID_DB_FIELD + "=?", new Object[]{user.getId()},
                (rs, rowNum) -> {
                    return mapTicketFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public Ticket findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + TICKET_DB_TABLE +
                        " WHERE " + ID_DB_FIELD + "=?",
                new Object[]{id},
                (rs, rowNum) -> {
                    return mapTicketFromRs(rs);
                });
    }

    private Ticket mapTicketFromRs(ResultSet rs) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setUser(userRepository.findById(rs.getInt(USER_ID_DB_FIELD)));
        ticket.setPlannedEvent(plannedEventRepository.findById(rs.getInt(PLANNED_EVENT_ID_DB_FIELD)));
        ticket.setSeat(rs.getInt(SEAT_DB_FIELD));
        ticket.setPrice(rs.getDouble(PRICE_DB_FIELD));
        ticket.setId(rs.getInt(ID_DB_FIELD));

        return ticket;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean isPurchased(PlannedEvent plannedEvent, int seat) {
        int count = jdbcTemplate.queryForObject("SELECT count(*) " +
                        "FROM " + TICKET_DB_TABLE + " " +
                        "WHERE " + PLANNED_EVENT_ID_DB_FIELD + "=? AND " + SEAT_DB_FIELD + "=?",
                new Object[]{plannedEvent.getId(), seat},
                Integer.class);

        return count != 0;
    }

    @Override
    public List<Ticket> findAll() {
        return jdbcTemplate.query("SELECT * FROM " + TICKET_DB_TABLE,
                (rs, rowNum) -> {
                    return mapTicketFromRs(rs);
                });
    }
}
