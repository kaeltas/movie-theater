package ua.pp.kaeltas.repository.ticket;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;

import java.time.LocalDateTime;
import java.util.List;

public interface TicketRepository {

    void save(Ticket ticket);
    List<Ticket> findAllByEvent(Event event, LocalDateTime dateTime);
    List<Ticket> findAllByUser(User user);

    Ticket findById(int id);

    boolean isPurchased(PlannedEvent plannedEvent, int seat);

    List<Ticket> findAll();
}
