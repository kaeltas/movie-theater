package ua.pp.kaeltas.repository.ticket;

import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ua.pp.kaeltas.config.ProfileInMemory;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.exception.NoSuchTicketException;

@Repository
@ProfileInMemory
public class InMemoryTicketRepository implements TicketRepository {
    private List<Ticket> tickets = new ArrayList<>();

    @Override
    public void save(Ticket ticket) {
        tickets.add(ticket);
    }

    @Override
    public List<Ticket> findAllByEvent(Event event, LocalDateTime dateTime) {
        return tickets.stream()
                .filter(p -> event.equals(p.getPlannedEvent().getEvent())
                        && dateTime.equals(p.getPlannedEvent().getDateTime()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Ticket> findAllByUser(User user) {
        return tickets.stream()
                .filter(t -> t.getUser().equals(user))
                .collect(Collectors.toList());
    }

    @Override
    public Ticket findById(int id) {
        return tickets.stream()
                .filter(t -> id == t.getId())
                .findFirst()
                .orElseThrow(() -> new NoSuchTicketException(id));
    }

    @Override
    public boolean isPurchased(PlannedEvent plannedEvent, int seat) {
        return tickets.stream()
                .anyMatch(t -> plannedEvent.equals(t.getPlannedEvent()) && seat == t.getSeat());
    }

    @Override
    public List<Ticket> findAll() {
        return tickets.stream()
                .collect(Collectors.toList());
    }

}
