package ua.pp.kaeltas.repository.user;

import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.UserAccount;

public interface UserAccountRepository {
    void save(UserAccount userAccount);
    void remove(Integer id);
    UserAccount findById(Integer id);
    UserAccount findByUser(User user);
}
