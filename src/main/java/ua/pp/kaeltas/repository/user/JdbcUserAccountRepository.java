package ua.pp.kaeltas.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.UserAccount;

@Repository
@ProfileJdbc
@Transactional
public class JdbcUserAccountRepository implements UserAccountRepository {
    /*
        Mappings for field names in DB
     */
    private static final String USER_ACCOUNT_DB_TABLE = "user_account";
    private static final String ID_DB_FIELD = "id";
    private static final String USER_ID_DB_FIELD = "user_id";
    private static final String AMOUNT_DB_FIELD = "amount";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(UserAccount userAccount) {
        if (null == userAccount.getId()) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(USER_ID_DB_FIELD, userAccount.getUser().getId());
            params.put(AMOUNT_DB_FIELD, userAccount.getAmount());
            Number id = simpleJdbcInsert
                    .withTableName(USER_ACCOUNT_DB_TABLE)
                    .usingGeneratedKeyColumns(ID_DB_FIELD)
                    .executeAndReturnKey(params);

            userAccount.setId(id.intValue());
        } else {
            jdbcTemplate.update("UPDATE " + USER_ACCOUNT_DB_TABLE + " " +
                            "SET " +
                            USER_ID_DB_FIELD + "=?, " +
                            AMOUNT_DB_FIELD + "=? " +
                            "WHERE " +
                            ID_DB_FIELD + "=?",
                    userAccount.getUser().getId(),
                    userAccount.getAmount(),
                    userAccount.getId());
        }
    }

    @Override
    public void remove(Integer id) {
        jdbcTemplate.update("DELETE FROM " + USER_ACCOUNT_DB_TABLE + " WHERE " + ID_DB_FIELD + "=?", id);
    }

    @Override
    @Transactional(readOnly = true)
    public UserAccount findById(Integer id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + USER_ACCOUNT_DB_TABLE + " WHERE " + ID_DB_FIELD + "=?", new Object[]{id},
                (rs, rowNum) -> {
                    return mapUserAccountFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true, noRollbackFor = {EmptyResultDataAccessException.class})
    public UserAccount findByUser(User user) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + USER_ACCOUNT_DB_TABLE + " WHERE " + USER_ID_DB_FIELD + "=?", new Object[]{user.getId()},
                (rs, rowNum) -> {
                    return mapUserAccountFromRs(rs);
                });
    }

    private UserAccount mapUserAccountFromRs(ResultSet rs) throws SQLException {
        UserAccount userAccount = new UserAccount();
        userAccount.setId(rs.getInt(ID_DB_FIELD));
        userAccount.setUser(userRepository.findById(rs.getInt(USER_ID_DB_FIELD)));
        userAccount.setAmount(rs.getDouble(AMOUNT_DB_FIELD));
        return userAccount;
    }
}
