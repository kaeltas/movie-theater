package ua.pp.kaeltas.repository.user;

import ua.pp.kaeltas.domain.User;

import java.util.List;

public interface UserRepository {

    void save(User user);
    void remove(Integer id);
    User findById(Integer id);
    User findByEmail(String email);
    List<User> findAllByName(String name);
    List<User> findAll();
}
