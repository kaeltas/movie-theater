package ua.pp.kaeltas.repository.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.pp.kaeltas.config.ProfileJdbc;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.role.RoleRepository;

@Repository
@ProfileJdbc
@Transactional
public class JdbcUserRepository implements UserRepository {
    /*
        Mappings for field names in DB
     */
    private static final String USER_DB_TABLE = "user";
    private static final String ID_DB_FIELD = "id";
    private static final String NAME_DB_FIELD = "name";
    private static final String EMAIL_DB_FIELD = "email";
    private static final String BIRTHDAY_DB_FIELD = "birthday";
    private static final String PASSWORD_HASH_DB_FIELD = "password_hash";
    private static final String ROLES_DB_FIELD = "roles";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void save(User user) {
        if (null == user.getId()) {
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);

            Map<String, Object> params = new HashMap<>();
            params.put(NAME_DB_FIELD, user.getName());
            params.put(EMAIL_DB_FIELD, user.getEmail());
            params.put(BIRTHDAY_DB_FIELD, Timestamp.valueOf(user.getBirthDay().atTime(0, 0)));
            params.put(PASSWORD_HASH_DB_FIELD, user.getPasswordHash());
            Number id = simpleJdbcInsert
                    .withTableName(USER_DB_TABLE)
                    .usingGeneratedKeyColumns(ID_DB_FIELD)
                    .executeAndReturnKey(params);

            user.setId(id.intValue());
        } else {
            jdbcTemplate.update("UPDATE " + USER_DB_TABLE + " " +
                            "SET " +
                            NAME_DB_FIELD + "=?, " +
                            EMAIL_DB_FIELD + "=?, " +
                            BIRTHDAY_DB_FIELD + "=?, " +
                            PASSWORD_HASH_DB_FIELD + "=?, " +
                            "WHERE " +
                            ID_DB_FIELD + "=?",
                    user.getName(),
                    user.getEmail(),
                    Timestamp.valueOf(user.getBirthDay().atTime(0, 0)),
                    user.getPasswordHash(),
                    user.getId());
        }
    }

    @Override
    public void remove(Integer id) {
        jdbcTemplate.update("DELETE FROM " + USER_DB_TABLE + " WHERE " + ID_DB_FIELD + "=?", id);
    }

    @Override
    @Transactional(readOnly = true)
    public User findById(Integer id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + USER_DB_TABLE + " WHERE " + ID_DB_FIELD + "=?", new Object[]{id},
                (rs, rowNum) -> {
                    return mapUserFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + USER_DB_TABLE + " WHERE " + EMAIL_DB_FIELD + "=?", new Object[]{email},
                (rs, rowNum) -> {
                    return mapUserFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAllByName(String name) {
        return jdbcTemplate.query("SELECT * FROM " + USER_DB_TABLE + " WHERE " + NAME_DB_FIELD + "=?", new Object[]{name},
                (rs, rowNum) -> {
                    return mapUserFromRs(rs);
                });
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return jdbcTemplate.query("SELECT * FROM " + USER_DB_TABLE,
                (rs, rowNum) -> {
                    return mapUserFromRs(rs);
                });
    }

    private User mapUserFromRs(ResultSet rs) throws SQLException {
        User user = new User(rs.getString(NAME_DB_FIELD),
                rs.getString(EMAIL_DB_FIELD),
                rs.getDate(BIRTHDAY_DB_FIELD).toLocalDate(),
                rs.getString(PASSWORD_HASH_DB_FIELD));
        user.setId(rs.getInt(ID_DB_FIELD));
        user.setRoles(roleRepository.findAllByUsername(user.getName()));
        return user;
    }
}
