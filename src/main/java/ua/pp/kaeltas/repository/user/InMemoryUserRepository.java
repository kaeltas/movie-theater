package ua.pp.kaeltas.repository.user;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import ua.pp.kaeltas.config.ProfileInMemory;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.exception.NoSuchUserException;

@Repository
@ProfileInMemory
public class InMemoryUserRepository implements UserRepository {
    private Map<Integer, User> usersMap = new ConcurrentHashMap<>();
    private AtomicInteger idCounter = new AtomicInteger();

    @Override
    public void save(User user) {
        Integer id = idCounter.incrementAndGet();
        user.setId(id);
        usersMap.put(id, user);
    }

    @Override
    public void remove(Integer id) {
        usersMap.remove(id);
    }

    @Override
    public User findById(Integer id) {
        User user = usersMap.get(id);
        if (null == user) {
            throw new NoSuchUserException("No such user with id=" + id);
        }
        return user;
    }

    @Override
    public User findByEmail(String email) {
        return usersMap.entrySet()
                .stream()
                .filter(p -> email.equals(p.getValue().getEmail()))
                .findFirst()
                .orElseThrow(() -> new NoSuchUserException("No such user with email=" + email))
                .getValue();
    }

    @Override
    public List<User> findAllByName(String name) {
        return usersMap.entrySet()
                .stream()
                .filter(p -> name.equals(p.getValue().getName()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> findAll() {
        return usersMap.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }
}
