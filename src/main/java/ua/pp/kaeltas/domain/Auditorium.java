package ua.pp.kaeltas.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

@Data
@EqualsAndHashCode(exclude = "id")
public class Auditorium {

    private Integer id;

    private String name;
    private int totalNumberOfSeats;
    private Set<Integer> vipSeats;

}
