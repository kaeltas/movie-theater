package ua.pp.kaeltas.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Role {
    public static final String REGISTERED_USER = "REGISTERED_USER";
    public static final String BOOKING_MANAGER = "BOOKING_MANAGER";

    private Integer id;
    private String userName;
    private String roleName;

    public Role(String userName, String roleName) {
        this.userName = userName;
        this.roleName = roleName;
    }
}
