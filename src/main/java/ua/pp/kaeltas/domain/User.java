package ua.pp.kaeltas.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = "id")
public class User {

    private Integer id;

    private String name;
    private String email;

    private String passwordHash;
    private List<Role> roles;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate birthDay;

    public User (String name, String email, LocalDate birthDay) {
        this.name = name;
        this.email = email;
        this.birthDay = birthDay;
        this.roles = Arrays.asList(new Role[] {new Role(name, Role.REGISTERED_USER)});
    }

    public User (String name, String email, LocalDate birthDay, String passwordHash) {
        this(name, email, birthDay);
        this.passwordHash = passwordHash;
    }

}
