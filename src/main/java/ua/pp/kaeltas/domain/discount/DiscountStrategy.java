package ua.pp.kaeltas.domain.discount;

import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;

import java.util.List;

@FunctionalInterface
public interface DiscountStrategy {
    double getDiscount(User user, List<Ticket> tickets);
}
