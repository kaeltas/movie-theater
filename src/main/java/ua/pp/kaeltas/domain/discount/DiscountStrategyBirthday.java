package ua.pp.kaeltas.domain.discount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.service.TicketPriceCalculator;

import java.time.LocalDate;
import java.util.List;

@Component
public class DiscountStrategyBirthday implements DiscountStrategy {
    private static final int BIRTHDAY_DISCOUNT_PERCENT = 5;

    @Autowired
    private TicketPriceCalculator ticketPriceCalculator;

    @Override
    public double getDiscount(User user, List<Ticket> tickets) {
        if (null != user) {
            double ticketsSumPrice = tickets.stream().mapToDouble(Ticket::getPrice).sum();
            if (isBirthday(user)) {
                return ticketsSumPrice * BIRTHDAY_DISCOUNT_PERCENT / 100.0;
            }
        }
        return 0;
    }

    private boolean isBirthday(User user) {
        return user.getBirthDay().getDayOfMonth() == LocalDate.now().getDayOfMonth()
                && user.getBirthDay().getMonth() == LocalDate.now().getMonth();
    }

}
