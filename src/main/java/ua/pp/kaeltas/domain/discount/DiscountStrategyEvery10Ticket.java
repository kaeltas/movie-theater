package ua.pp.kaeltas.domain.discount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.ticket.TicketRepository;
import ua.pp.kaeltas.service.TicketPriceCalculator;

@Component
public class DiscountStrategyEvery10Ticket implements DiscountStrategy {
    private static final int EVERY_10_TICKET_DISCOUNT_PERCENT = 50;

    @Autowired
    private TicketPriceCalculator ticketPriceCalculator;

    @Autowired
    private TicketRepository ticketRepository;

    @Override
    public double getDiscount(User user, List<Ticket> tickets) {
        double ticketsSumDiscount = 0;
        List<Ticket> allTicketsByUser = ticketRepository.findAllByUser(user);

        int ticketNumber = allTicketsByUser.size();
        for (Ticket ticket : tickets) {
            ticketNumber++;
            if (ticketNumber % 10 == 0) {
                ticketsSumDiscount += ticket.getPrice() * EVERY_10_TICKET_DISCOUNT_PERCENT / 100.0;
            }
        }

        return ticketsSumDiscount;
    }
}
