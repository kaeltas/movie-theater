package ua.pp.kaeltas.domain;

import lombok.Data;

@Data
public class UserAccount {

    private Integer id;
    private User user;
    private double amount;

}
