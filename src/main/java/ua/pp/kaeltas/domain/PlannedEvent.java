package ua.pp.kaeltas.domain;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude = "id")
public class PlannedEvent {

    private Integer id;

    private Event event;
    private Auditorium auditorium;
    private LocalDateTime dateTime;

    public PlannedEvent(Event event, Auditorium auditorium, LocalDateTime dateTime) {
        this.event = event;
        this.auditorium = auditorium;
        this.dateTime = dateTime;
    }

}
