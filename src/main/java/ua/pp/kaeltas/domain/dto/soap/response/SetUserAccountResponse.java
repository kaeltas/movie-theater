package ua.pp.kaeltas.domain.dto.soap.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "result"
})
@XmlRootElement(namespace = "http://ua.pp.kaeltas/movie-theater", name = "setUserAccountResponse")
public class SetUserAccountResponse {
    private String result;

    public SetUserAccountResponse() {
    }

    public SetUserAccountResponse(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
