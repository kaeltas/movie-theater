package ua.pp.kaeltas.domain.dto.rest;

import lombok.Data;
import ua.pp.kaeltas.domain.UserAccount;

@Data
public class UserAccountResponseDto {
    private Integer id;
    private UserResponseDto user;
    private double amount;

    public UserAccountResponseDto(UserAccount userAccount) {
        this.id = userAccount.getId();
        this.user = new UserResponseDto(userAccount.getUser());
        this.amount = userAccount.getAmount();
    }
}
