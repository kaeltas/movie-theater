package ua.pp.kaeltas.domain.dto.rest;

import lombok.Data;

@Data
public class TicketForBookingRequestDto {
    private Integer userId;
    private Integer plannedEventId;
    private Integer seat;
}
