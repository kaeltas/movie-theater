package ua.pp.kaeltas.domain.dto.soap.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ua.pp.kaeltas.domain.dto.soap.PlannedEventPayload;
import ua.pp.kaeltas.domain.dto.soap.UserPayload;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "user",
        "plannedEvent",
        "seat",
        "price"
})
@XmlRootElement(namespace = "http://ua.pp.kaeltas/movie-theater", name = "bookTicketResponse")
public class BookTicketResponse {
    private UserPayload user;
    private PlannedEventPayload plannedEvent;
    private Integer seat;
    private Double price;

    public UserPayload getUser() {
        return user;
    }

    public void setUser(UserPayload user) {
        this.user = user;
    }

    public PlannedEventPayload getPlannedEvent() {
        return plannedEvent;
    }

    public void setPlannedEvent(PlannedEventPayload plannedEvent) {
        this.plannedEvent = plannedEvent;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
