package ua.pp.kaeltas.domain.dto.rest;

import lombok.Data;

@Data
public class UserAccountRequestDto {
    private Integer userId;
    private double amount;
}
