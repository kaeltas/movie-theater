package ua.pp.kaeltas.domain.dto.rest;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Data;
import ua.pp.kaeltas.domain.Role;
import ua.pp.kaeltas.domain.User;

@Data
public class UserResponseDto {

    private Integer id;

    private String name;
    private String email;

    private List<String> roles;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate birthDay;

    public UserResponseDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.roles = user.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList());
        this.birthDay = user.getBirthDay();
    }
}
