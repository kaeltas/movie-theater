package ua.pp.kaeltas.domain.dto.soap.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "userId",
        "plannedEventId",
        "seat"
})
@XmlRootElement(namespace = "http://ua.pp.kaeltas/movie-theater", name = "bookTicketRequest")
public class BookTicketRequest {
    @XmlElement(required = true)
    private Integer userId;
    @XmlElement(required = true)
    private Integer plannedEventId;
    @XmlElement(required = true)
    private Integer seat;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPlannedEventId() {
        return plannedEventId;
    }

    public void setPlannedEventId(Integer plannedEventId) {
        this.plannedEventId = plannedEventId;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }
}
