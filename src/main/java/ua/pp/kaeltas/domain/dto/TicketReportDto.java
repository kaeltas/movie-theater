package ua.pp.kaeltas.domain.dto;

import java.time.LocalDateTime;

import lombok.Data;
import ua.pp.kaeltas.domain.Ticket;

@Data
public class TicketReportDto {
    private String userName;
    private String eventName;
    private LocalDateTime eventDateTime;
    private String eventAuditorium;
    private int seat;
    private double price;

    public TicketReportDto(Ticket ticket) {
        this.userName = ticket.getUser().getName();
        this.eventName = ticket.getPlannedEvent().getEvent().getName();
        this.eventAuditorium = ticket.getPlannedEvent().getAuditorium().getName();
        this.eventDateTime = ticket.getPlannedEvent().getDateTime();
        this.seat = ticket.getSeat();
        this.price = ticket.getPrice();
    }
}
