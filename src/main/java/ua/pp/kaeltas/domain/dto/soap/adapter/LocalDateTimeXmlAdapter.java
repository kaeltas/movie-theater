package ua.pp.kaeltas.domain.dto.soap.adapter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;

@XmlTransient
public class LocalDateTimeXmlAdapter extends XmlAdapter<Date, LocalDateTime> {

    @Override
    public LocalDateTime unmarshal(Date date) throws Exception {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    @Override
    public Date marshal(LocalDateTime localDateTime) throws Exception {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}