package ua.pp.kaeltas.domain.dto.rest;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

import lombok.Data;
import ua.pp.kaeltas.domain.EventRating;
import ua.pp.kaeltas.domain.Ticket;

@Data
public class TicketResponseDto {

    private Integer id;

    private String userName;
    private String userEmail;

    private String eventName;
    private EventRating eventRating;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    private LocalDateTime eventDateTime;

    private String auditoriumName;
    private int seat;
    private double price;

    public TicketResponseDto(Ticket ticket) {
        this.id = ticket.getId();
        this.userName = ticket.getUser().getName();
        this.userEmail = ticket.getUser().getEmail();
        this.eventName = ticket.getPlannedEvent().getEvent().getName();
        this.eventRating = ticket.getPlannedEvent().getEvent().getRating();
        this.eventDateTime = ticket.getPlannedEvent().getDateTime();
        this.auditoriumName = ticket.getPlannedEvent().getAuditorium().getName();
        this.seat = ticket.getSeat();
        this.price = ticket.getPrice();
    }

}
