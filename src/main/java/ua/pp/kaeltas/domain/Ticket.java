package ua.pp.kaeltas.domain;

import lombok.Data;

@Data
public class Ticket {

    private Integer id;

    private User user;
    private PlannedEvent plannedEvent;
    private int seat;
    private double price;

}
