package ua.pp.kaeltas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.EventRating;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Role;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.user.UserAccountRepository;
import ua.pp.kaeltas.service.AuditoriumService;
import ua.pp.kaeltas.service.BookingService;
import ua.pp.kaeltas.service.EventService;
import ua.pp.kaeltas.service.UserService;

@Component
public class DataPopulator {

    @Autowired
    private UserService userService;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private AuditoriumService auditoriumService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private EventService eventService;

    @Autowired
    private BookingService bookingService;

    @PostConstruct
    public void init() {
        populateTestData();
    }

    public void populateTestData() {

        registerUser("user", "user");
        registerUser("admin", "admin", Role.BOOKING_MANAGER, Role.REGISTERED_USER);
        User user = registerUser("paddy", "paddy");
        userService.deposit(user, 1000);
        Auditorium auditoriumMercury = getAuditorium();

        //Create events & assign auditoriums
        Event movieLoTR = createEvent("Movie: Lord Of The Rings", 100, EventRating.MID);
        Event movieHobbit = createEvent("Movie: The Hobbit", 10, EventRating.HIGH);

        LocalDateTime dateTimeLoTR = LocalDateTime.of(2016, Month.FEBRUARY, 8, 13, 0);
        eventService.assignAuditorium(movieLoTR, auditoriumMercury, dateTimeLoTR);
        LocalDateTime dateTimeHobbit = LocalDateTime.of(2016, Month.FEBRUARY, 8, 15, 0);
        eventService.assignAuditorium(movieHobbit, auditoriumMercury, dateTimeHobbit);

        //Create seats for booking
        Set<Integer> seatsHobbit = new HashSet<>();
        seatsHobbit.add(1);
        seatsHobbit.add(2);
        seatsHobbit.add(10);
        seatsHobbit.add(11);

        Set<Integer> seatsLoTR = new HashSet<>();
        for (int i = 1; i <= 10; i++) {
            seatsLoTR.add(i);
        }

        //Calc tickets price
        double ticketPriceHobbit = bookingService.getTicketPrice(movieHobbit, dateTimeHobbit, seatsHobbit, user);

        double ticketPriceLoTR = bookingService.getTicketPrice(movieLoTR, dateTimeLoTR, seatsLoTR, null);

        //Purchase tickets
        PlannedEvent plannedEvent = eventService.getPlannedEvent(movieHobbit, dateTimeHobbit);

        Ticket ticket1 = bookingService.createTicket(plannedEvent, 1);
        bookingService.bookTicket(user, ticket1);

        Ticket ticket2 = bookingService.createTicket(plannedEvent, 2);
        bookingService.bookTicket(user, ticket2);
    }

    private Event createEvent(String name, int basePrice, EventRating mid) {
        Event movieLoTR = new Event(name, basePrice, mid);
        eventService.create(movieLoTR);

        return movieLoTR;
    }

    private Auditorium getAuditorium() {
        Auditorium auditoriumMercury = auditoriumService.getAuditoriumByName("Mercury");
        System.out.println(auditoriumMercury);
        return auditoriumMercury;
    }

    private User registerUser(String name, String password) {
        LocalDate noBirthDayToday = LocalDate.now().withYear(1990).plusDays(1);
        User user = new User(name, name + "@mail.com", noBirthDayToday, bCryptPasswordEncoder.encode(password));
        userService.register(user);

        System.out.println(userService.getByEmail(name + "@mail.com"));

        return user;
    }

    private User registerUser(String name, String password, String ... roles) {
        LocalDate noBirthDayToday = LocalDate.now().withYear(1990).plusDays(1);
        User user = new User(name, name + "@mail.com", noBirthDayToday, bCryptPasswordEncoder.encode(password));

        List<Role> listRoles = new ArrayList<>();
        for (String role : roles) {
            listRoles.add(new Role(name, role));
        }

        user.setRoles(listRoles);
        userService.register(user);

        System.out.println(userService.getByEmail(name + "@mail.com"));

        return user;
    }
}
