package ua.pp.kaeltas.parser;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Type {
    public static final String NODE_NAME = "type";

    private ParseType parseType;
    private String version;

}
