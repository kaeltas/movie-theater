package ua.pp.kaeltas.parser;

public enum ParseType {
    USERS("users"), EVENTS("events");

    private String nodeName;

    ParseType(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getNodeName() {
        return nodeName;
    }
}
