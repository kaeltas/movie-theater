package ua.pp.kaeltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.service.EventService;

@Slf4j
@Controller
@RequestMapping("event")
public class EventController {

    @Autowired
    private EventService eventService;

    @RequestMapping()
    public String getAllEvents(Model model) {
        List<Event> events = eventService.getAll();
        model.addAttribute("events", events);

        Map<Integer, List<PlannedEvent>> plannedEvents = new HashMap<>();
        for (Event event : events) {
            plannedEvents.put(event.getId(), eventService.getPlannedEvents(event));
        }

        log.debug("plannedEvents: {}", plannedEvents);

        model.addAttribute("plannedEvents", plannedEvents);

        return "events";
    }

}
