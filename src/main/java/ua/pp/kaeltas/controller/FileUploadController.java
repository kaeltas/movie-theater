package ua.pp.kaeltas.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.parser.ParseType;
import ua.pp.kaeltas.parser.Type;
import ua.pp.kaeltas.service.EventService;
import ua.pp.kaeltas.service.UserService;

@Slf4j
@Controller
public class FileUploadController {

    @Autowired
    private UserService userService;

    @Autowired
    private EventService eventService;

    private static List<String> filesList = new LinkedList<>();

    @RequestMapping(method = RequestMethod.GET, value = "/upload")
    public String provideUploadInfo(Model model) {
        model.addAttribute("files", filesList);

        return "uploadForm";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile[] files,
                                   RedirectAttributes redirectAttributes) throws IOException {

        StringBuilder processedFiles = new StringBuilder();

        for (MultipartFile file : files) {
            if (!file.isEmpty()) {
                processFile(file);

                processedFiles
                        .append(file.getOriginalFilename())
                        .append(", ");
            }
        }
        if (processedFiles.length() > 0) {
            processedFiles.delete(processedFiles.length() - 2, processedFiles.length());
        }

        redirectAttributes.addFlashAttribute("message",
                "Files successfully processed: [" + processedFiles.toString() + "]");

        return "redirect:upload";
    }

    private void processFile(MultipartFile file) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        JsonNode rootNode = mapper.readTree(file.getInputStream());

        Type type = mapper.treeToValue(rootNode.get(Type.NODE_NAME), Type.class);
        log.debug("Processing fileType: {}", type);
        switch (type.getParseType()) {
            case USERS: {
                User[] users = mapper.treeToValue(rootNode.get(ParseType.USERS.getNodeName()), User[].class);
                log.debug("Parsed users: {}", Arrays.toString(users));
                for (User user : users) {
                    userService.register(user);
                }

                break;
            }
            case EVENTS: {
                Event[] events = mapper.treeToValue(rootNode.get(ParseType.EVENTS.getNodeName()), Event[].class);
                log.debug("Parsed events: {}", Arrays.toString(events));
                for (Event event : events) {
                    eventService.create(event);
                }
                break;
            }
            default: {
                throw new RuntimeException("Unsupported filetype for import: " + type.toString());
            }
        }
    }

}
