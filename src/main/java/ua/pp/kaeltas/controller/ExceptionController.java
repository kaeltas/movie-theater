package ua.pp.kaeltas.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Controller
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    public String viewException(Model model, Exception exception){
        model.addAttribute("error", exception.getMessage());
        model.addAttribute("exception", exception);

        return "error";
    }

    @RequestMapping(value = "/access-denied", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String accessDeniedHandler(Model model) {
        model.addAttribute("error", "Access to this resource is denied!");

        return "error";
    }

}
