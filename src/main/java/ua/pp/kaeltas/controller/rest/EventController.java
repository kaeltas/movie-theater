package ua.pp.kaeltas.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import lombok.extern.slf4j.Slf4j;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.service.EventService;

@Slf4j
@RestController("restEventController")
@RequestMapping("/rest/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @RequestMapping()
    public List<Event> getAllEvents() {
        return eventService.getAll();
    }

}
