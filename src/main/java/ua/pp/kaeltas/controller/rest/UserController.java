package ua.pp.kaeltas.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.UserAccount;
import ua.pp.kaeltas.domain.dto.rest.UserAccountRequestDto;
import ua.pp.kaeltas.domain.dto.rest.UserAccountResponseDto;
import ua.pp.kaeltas.domain.dto.rest.UserResponseDto;
import ua.pp.kaeltas.repository.user.UserAccountRepository;
import ua.pp.kaeltas.service.UserService;

@RestController("userRestController")
@RequestMapping("/rest/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @RequestMapping()
    public List<UserResponseDto> getAllUsers() {
        return userService.getAll().stream()
                .map(UserResponseDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{userid}/account", method = RequestMethod.GET)
    public UserAccountResponseDto deposit(Model model, @PathVariable("userid") User user) {
        return new UserAccountResponseDto(userService.getUserAccount(user));
    }

    @RequestMapping(value = "/{userid}/account", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void depositPost(@RequestBody UserAccountRequestDto userAccountReq, @PathVariable("userid") User user) {
        UserAccount userAccount = userService.getUserAccount(user);
        userAccount.setUser(user);
        userAccount.setAmount(userAccountReq.getAmount());
        userAccountRepository.save(userAccount);
    }

}
