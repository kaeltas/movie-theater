package ua.pp.kaeltas.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.dto.rest.TicketForBookingRequestDto;
import ua.pp.kaeltas.domain.dto.rest.TicketResponseDto;
import ua.pp.kaeltas.service.BookingService;
import ua.pp.kaeltas.service.EventService;

@RestController("restBookingController")
@RequestMapping("/rest/ticket")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private EventService eventService;

    @RequestMapping
    public List<TicketResponseDto> getAllTickets() {
        return bookingService.getAllTickets();
    }

    @RequestMapping("/{ticketId}")
    public TicketResponseDto getTicketById(@PathVariable int ticketId) {
        return bookingService.getTicket(ticketId);
    }


    @RequestMapping(params = {"userid"}, method = RequestMethod.GET, produces = "text/html")
    public List<TicketResponseDto> getTicketsByUser(@RequestParam("userid") User user) {
        return bookingService.getPurchasedTickets(user).stream()
                .map(TicketResponseDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(params = {"eventid", "datetime"})
    public List<TicketResponseDto> getPurchasedTicketsForEvent(@RequestParam("eventid") Event event, @RequestParam("datetime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime localDateTime, Model model) {
        return bookingService.getPurchasedTicketsForEvent(event, localDateTime).stream()
                .map(TicketResponseDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/book", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void bookTicket(@RequestBody TicketForBookingRequestDto ticketForBookingRequestDto) {
        bookingService.bookTicket(ticketForBookingRequestDto);
    }

}
