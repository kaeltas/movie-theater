package ua.pp.kaeltas.controller.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.dto.soap.request.GetUserAccountRequest;
import ua.pp.kaeltas.domain.dto.soap.request.SetUserAccountRequest;
import ua.pp.kaeltas.domain.dto.soap.response.GetUserAccountResponse;
import ua.pp.kaeltas.domain.dto.soap.response.SetUserAccountResponse;
import ua.pp.kaeltas.service.UserService;

import static ua.pp.kaeltas.controller.soap.Converter.convert;

@Endpoint
public class UserAccountEndpoint {
    private static final String NAMESPACE_URI = "http://ua.pp.kaeltas/movie-theater";

    @Autowired
    private UserService userService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserAccountRequest")
    @ResponsePayload
    public GetUserAccountResponse getUserAccount(@RequestPayload GetUserAccountRequest request) {
        User user = userService.getById(request.getUserId());

        return convert(userService.getUserAccount(user));
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "setUserAccountRequest")
    @ResponsePayload
    public SetUserAccountResponse setUserAccount(@RequestPayload SetUserAccountRequest request) {
        User user = userService.getById(request.getUserId());
        userService.deposit(user, request.getAmount());

        return new SetUserAccountResponse("AccountId " + user.getId() + " was successfully updated");
    }

}
