package ua.pp.kaeltas.controller.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ua.pp.kaeltas.domain.dto.soap.request.BookTicketRequest;
import ua.pp.kaeltas.domain.dto.soap.response.BookTicketResponse;
import ua.pp.kaeltas.service.BookingService;

import static ua.pp.kaeltas.controller.soap.Converter.convert;

@Endpoint
public class BookingEndpoint {
    private static final String NAMESPACE_URI = "http://ua.pp.kaeltas/movie-theater";

    @Autowired
    private BookingService bookingService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "bookTicketRequest")
    @ResponsePayload
    public BookTicketResponse bookTicket(@RequestPayload BookTicketRequest request) {
        return convert(bookingService.bookTicket(request));
    }

}
