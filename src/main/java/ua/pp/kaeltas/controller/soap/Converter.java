package ua.pp.kaeltas.controller.soap;

import java.util.stream.Collectors;

import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Role;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.UserAccount;
import ua.pp.kaeltas.domain.dto.soap.PlannedEventPayload;
import ua.pp.kaeltas.domain.dto.soap.UserPayload;
import ua.pp.kaeltas.domain.dto.soap.response.BookTicketResponse;
import ua.pp.kaeltas.domain.dto.soap.response.GetUserAccountResponse;

public class Converter {

    public static GetUserAccountResponse convert(UserAccount userAccount) {
        GetUserAccountResponse result = new GetUserAccountResponse();
        result.setId(userAccount.getId());

        result.setUser(convert(userAccount.getUser()));
        result.setAmount(userAccount.getAmount());

        return result;
    }

    public static UserPayload convert(User user) {
        UserPayload result = new UserPayload();
        result.setId(user.getId());
        result.setName(user.getName());
        result.setEmail(user.getEmail());
        result.setRoles(user.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList()));

        return result;
    }

    public static BookTicketResponse convert(Ticket ticket) {
        BookTicketResponse result = new BookTicketResponse();
        result.setUser(convert(ticket.getUser()));
        result.setPlannedEvent(convert(ticket.getPlannedEvent()));
        result.setPrice(ticket.getPrice());
        result.setSeat(ticket.getSeat());

        return result;
    }

    public static PlannedEventPayload convert(PlannedEvent plannedEvent) {
        PlannedEventPayload result = new PlannedEventPayload();
        result.setId(plannedEvent.getId());
        result.setAuditoriumName(plannedEvent.getAuditorium().getName());
        result.setEventName(plannedEvent.getEvent().getName());

        return result;
    }

}
