package ua.pp.kaeltas.controller;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.dto.TicketReportDto;
import ua.pp.kaeltas.service.BookingService;
import ua.pp.kaeltas.service.EventService;

@Controller
@RequestMapping("tickets")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private EventService eventService;

    @RequestMapping(params = {"userid"}, method = RequestMethod.GET, produces = "text/html")
    public String getTicketsByUser(@RequestParam("userid") User user, Model model) {
        List<Ticket> tickets = bookingService.getPurchasedTickets(user);
        model.addAttribute("tickets", tickets);
        model.addAttribute("user", user);

        return "tickets";
    }

    @RequestMapping(value = {"", "/pdf"}, params = {"userid"}, method = RequestMethod.GET, produces = "application/pdf")
    public String getTicketsByUserPdf(@RequestParam("userid") User user, Model model) {
        List<Ticket> tickets = bookingService.getPurchasedTickets(user);
        model.addAttribute("tickets", tickets);

        List<TicketReportDto> ticketBeans = tickets.stream()
                .map(t -> new TicketReportDto(t))
                .collect(Collectors.toList());

        model.addAttribute("datasource", new JRBeanCollectionDataSource(ticketBeans));
        model.addAttribute("format", "pdf");
        return "rpt_tickets";
    }

    @RequestMapping(params = {"eventid", "datetime"})
    public String getPurchasedTicketsForEvent(@RequestParam("eventid") Event event, @RequestParam("datetime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime localDateTime, Model model) {
        model.addAttribute("tickets", bookingService.getPurchasedTicketsForEvent(event, localDateTime));
        model.addAttribute("plannedEvent", eventService.getPlannedEvent(event, localDateTime));

        return "tickets";
    }

    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public String bookTicket(@RequestParam("peventid") PlannedEvent plannedEvent, Model model) {
        model.addAttribute("plannedEvent", plannedEvent);

        return "bookform";
    }

    @RequestMapping(value = "/book", method = RequestMethod.POST)
    public String bookTicket(@RequestParam("userid") User user,
                             @RequestParam("peventid")PlannedEvent plannedEvent,
                             @RequestParam("seat") Integer seat,
                             Model model) {
        if (null == user) {
            throw new RuntimeException("User can't be empty");
        }
        Ticket ticket = bookingService.createTicket(plannedEvent, seat);
        bookingService.bookTicket(user, ticket);

        return "redirect:/tickets?eventid="+plannedEvent.getEvent().getId()+"&datetime="+plannedEvent.getDateTime();
    }

}
