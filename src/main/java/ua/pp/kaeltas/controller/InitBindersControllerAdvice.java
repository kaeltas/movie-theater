package ua.pp.kaeltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.ticket.TicketRepository;
import ua.pp.kaeltas.service.EventService;
import ua.pp.kaeltas.service.UserService;

@ControllerAdvice
public class InitBindersControllerAdvice {

    @Autowired
    private UserService userService;

    @Autowired
    private EventService eventService;

    @Autowired
    private TicketRepository ticketRepository;

    @InitBinder
    private void userBinder(WebDataBinder binder) {
        binder.registerCustomEditor(User.class,
                new PropertyEditorSupport() {
                    @Override
                    public void setAsText(String userid) {
                        User user = null;
                        if (userid != null && !userid.trim().isEmpty()) {
                            Integer idInt = Integer.valueOf(userid);
                            user = userService.getById(idInt);
                        }
                        setValue(user);
                    }
                }
        );
    }

    @InitBinder
    private void eventBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Event.class,
                new PropertyEditorSupport() {
                    @Override
                    public void setAsText(String eventid) {
                        Event event = null;
                        if (eventid != null && !eventid.trim().isEmpty()) {
                            Integer idInt = Integer.valueOf(eventid);
                            event = eventService.getById(idInt);
                        }
                        setValue(event);
                    }
                }
        );
    }

    @InitBinder
    private void plannedEventBinder(WebDataBinder binder) {
        binder.registerCustomEditor(PlannedEvent.class,
                new PropertyEditorSupport() {
                    @Override
                    public void setAsText(String peventid) {
                        PlannedEvent plannedEvent = null;
                        if (peventid != null && !peventid.trim().isEmpty()) {
                            Integer idInt = Integer.valueOf(peventid);
                            plannedEvent = eventService.getPlannedEventById(idInt);
                        }
                        setValue(plannedEvent);
                    }
                }
        );
    }

    @InitBinder
    private void ticketBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Ticket.class,
                new PropertyEditorSupport() {
                    @Override
                    public void setAsText(String ticketid) {
                        Ticket ticket = null;
                        if (ticketid != null && !ticketid.trim().isEmpty()) {
                            Integer idInt = Integer.valueOf(ticketid);
                            ticket = ticketRepository.findById(idInt);
                        }
                        setValue(ticket);
                    }
                }
        );
    }

}
