package ua.pp.kaeltas.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.UserAccount;
import ua.pp.kaeltas.repository.user.UserAccountRepository;
import ua.pp.kaeltas.service.UserService;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @RequestMapping()
    public String getAllUsers(Model model) {
        model.addAttribute("users", userService.getAll());

        return "users";
    }

    @RequestMapping(value = "account", method = RequestMethod.GET)
    public String deposit(Model model, @RequestParam("userid") User user) {
        model.addAttribute("user", user);
        model.addAttribute("userAccount", userService.getUserAccount(user));

        return "account";
    }

    @RequestMapping(value = "account", method = RequestMethod.POST)
    public String depositPost(UserAccount userAccount, @RequestParam("userid") User user) {
        userAccount.setUser(user);
        userAccountRepository.save(userAccount);

        return "redirect:account?userid="+user.getId();
    }

}
