package ua.pp.kaeltas;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashSet;
import java.util.Set;

import ua.pp.kaeltas.config.ContextConfig;
import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.EventRating;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.service.AuditoriumService;
import ua.pp.kaeltas.service.BookingService;
import ua.pp.kaeltas.service.EventService;
import ua.pp.kaeltas.service.StatisticsService;
import ua.pp.kaeltas.service.UserService;

public class MainApp {

    public static void main(String[] args) {
        MainApp app = new MainApp();
        app.testApp();
    }

    private void testApp() {
        ConfigurableApplicationContext context =
                new AnnotationConfigApplicationContext(ContextConfig.class);

        User user = registerUser(context);
        Auditorium auditoriumMercury = getAuditorium(context);

        StatisticsService statisticsService = context.getBean(StatisticsService.class);

        EventService eventService = context.getBean(EventService.class);

        //Create events & assign auditoriums
        Event movieLoTR = createEvent(eventService, "Movie: Lord Of The Rings", 100, EventRating.MID);
        Event movieHobbit = createEvent(eventService, "Movie: The Hobbit", 10, EventRating.HIGH);

        LocalDateTime dateTimeLoTR = LocalDateTime.of(2016, Month.FEBRUARY, 8, 13, 0);
        eventService.assignAuditorium(movieLoTR, auditoriumMercury, dateTimeLoTR);
        LocalDateTime dateTimeHobbit = LocalDateTime.of(2016, Month.FEBRUARY, 8, 15, 0);
        eventService.assignAuditorium(movieHobbit, auditoriumMercury, dateTimeHobbit);

        System.out.println(">>>[Aspect] EventRequestsCount (Movie: Lord Of The Rings): " + statisticsService.getEventRequestsCount(movieLoTR));
        eventService.getByName("Movie: Lord Of The Rings");
        System.out.println(">>>[Aspect] EventRequestsCount (Movie: Lord Of The Rings): " + statisticsService.getEventRequestsCount(movieLoTR));
        eventService.getByName("Movie: Lord Of The Rings");
        System.out.println(">>>[Aspect] EventRequestsCount (Movie: Lord Of The Rings): " + statisticsService.getEventRequestsCount(movieLoTR));
        System.out.println(">>>[Aspect] TicketPriceRequestsCount (Movie: Lord Of The Rings): " + statisticsService.getEventTicketPriceRequestsCount(movieLoTR));
        System.out.println(">>>[Aspect] BookTicketRequestsCount (Movie: The Hobbit): " + statisticsService.getBookTicketRequestsCount(movieHobbit));
        System.out.println(">>>[Aspect] TotalDiscountRequestsCount: " + statisticsService.getTotalDiscountRequestsCount());
        System.out.println(">>>[Aspect] TotalDiscountRequestsCount (user): " + statisticsService.getUserDiscountRequestsCount(user));

        BookingService bookingService = context.getBean(BookingService.class);

        //Create seats for booking
        Set<Integer> seatsHobbit = new HashSet<>();
        seatsHobbit.add(1);
        seatsHobbit.add(2);
        seatsHobbit.add(10);
        seatsHobbit.add(11);

        Set<Integer> seatsLoTR = new HashSet<>();
        for (int i = 1; i <= 10; i++) {
            seatsLoTR.add(i);
        }

        //Calc tickets price
        double ticketPriceHobbit = bookingService.getTicketPrice(movieHobbit, dateTimeHobbit, seatsHobbit, user);
        System.out.println("Hobbit tickets price = " + ticketPriceHobbit);

        double ticketPriceLoTR = bookingService.getTicketPrice(movieLoTR, dateTimeLoTR, seatsLoTR, null);
        System.out.println("LoTR tickets price = " + ticketPriceLoTR);

        //Purchase tickets
        System.out.println("Purchased tickets for Hobbit: " +
                bookingService.getPurchasedTicketsForEvent(movieHobbit, dateTimeHobbit));

        PlannedEvent plannedEvent = eventService.getPlannedEvent(movieHobbit, dateTimeHobbit);

        Ticket ticket1 = bookingService.createTicket(plannedEvent, 1);
        bookingService.bookTicket(user, ticket1);

        Ticket ticket2 = bookingService.createTicket(plannedEvent, 2);
        bookingService.bookTicket(user, ticket2);

        System.out.println("Purchased tickets for Hobbit: " +
                bookingService.getPurchasedTicketsForEvent(movieHobbit, dateTimeHobbit));

        System.out.println(">>>[Aspect] TicketPriceRequestsCount (Movie: Lord Of The Rings): " + statisticsService.getEventTicketPriceRequestsCount(movieLoTR));
        System.out.println(">>>[Aspect] BookTicketRequestsCount (Movie: The Hobbit): " + statisticsService.getBookTicketRequestsCount(movieHobbit));
        System.out.println(">>>[Aspect] TotalDiscountRequestsCount: " + statisticsService.getTotalDiscountRequestsCount());
        System.out.println(">>>[Aspect] TotalDiscountRequestsCount (user): " + statisticsService.getUserDiscountRequestsCount(user));

        context.registerShutdownHook();
    }

    private Event createEvent(EventService eventService, String name, int basePrice, EventRating mid) {
        Event movieLoTR = new Event(name, basePrice, mid);
        eventService.create(movieLoTR);

        return movieLoTR;
    }

    private Auditorium getAuditorium(ConfigurableApplicationContext context) {
        AuditoriumService auditoriumService = context.getBean(AuditoriumService.class);
        Auditorium auditoriumMercury = auditoriumService.getAuditoriumByName("Mercury");
        System.out.println(auditoriumMercury);
        return auditoriumMercury;
    }

    private User registerUser(ConfigurableApplicationContext context) {
        UserService userService = context.getBean(UserService.class);
        LocalDate noBirthDayToday = LocalDate.now().withYear(1990).plusDays(1);
        User user = new User("Paddy", "paddy@mail.com", noBirthDayToday);
        userService.register(user);
        System.out.println(userService.getByEmail("paddy@mail.com"));

        return user;
    }

}
