package ua.pp.kaeltas.service;

import ua.pp.kaeltas.domain.Auditorium;

import java.util.List;
import java.util.Set;

public interface AuditoriumService {

    List<Auditorium> getAuditoriums();
    Auditorium getAuditoriumByName(String auditoriumName);
    int getSeatsNumber(String auditoriumName);
    Set<Integer> getVipSeats(String auditoriumName);

}
