package ua.pp.kaeltas.service;

import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;

import java.time.LocalDateTime;
import java.util.List;

public interface EventService {

    void create(Event event);
    void remove(String name);
    Event getByName(String name);
    Event getById(Integer eventId);
    List<Event> getAll();

    List<PlannedEvent> getForDateRange(LocalDateTime from, LocalDateTime to);
    void assignAuditorium(Event event, Auditorium auditorium, LocalDateTime date);

    PlannedEvent getPlannedEvent(Event event, LocalDateTime dateTime);
    List<PlannedEvent> getPlannedEvents(Event event);

    PlannedEvent getPlannedEventById(Integer id);
}
