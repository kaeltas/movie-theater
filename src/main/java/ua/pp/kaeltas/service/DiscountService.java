package ua.pp.kaeltas.service;

import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;

import java.util.List;

@FunctionalInterface
public interface DiscountService {

    /**
     * Returns sum discount for all tickets in list for the user
     */
    double getDiscount(User user, List<Ticket> tickets);
}
