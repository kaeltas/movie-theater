package ua.pp.kaeltas.service.impl;

import org.springframework.stereotype.Service;
import ua.pp.kaeltas.domain.EventRating;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.service.TicketPriceCalculator;

@Service
public class TicketPriceCalculatorImpl implements TicketPriceCalculator {

    @Override
    public double calcTicketPrice(PlannedEvent plannedEvent, int seat) {
        double ticketPrice = plannedEvent.getEvent().getBasePrice();
        if (EventRating.HIGH == plannedEvent.getEvent().getRating()) {
            ticketPrice *= 1.2;
        }

        if (plannedEvent.getAuditorium().getVipSeats().contains(seat)) {
            ticketPrice *= 2;
        }

        return ticketPrice;
    }

}
