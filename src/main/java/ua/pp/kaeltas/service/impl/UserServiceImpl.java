package ua.pp.kaeltas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.UserAccount;
import ua.pp.kaeltas.exception.InsufficientFundsException;
import ua.pp.kaeltas.repository.role.RoleRepository;
import ua.pp.kaeltas.repository.user.UserAccountRepository;
import ua.pp.kaeltas.repository.user.UserRepository;
import ua.pp.kaeltas.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void register(User user) {
        userRepository.save(user);
        user.getRoles()
                .stream()
                .forEach(roleRepository::save);
    }

    @Override
    public void remove(Integer id) {
        userRepository.remove(id);
    }

    @Override
    public void remove(User user) {
        userRepository.remove(user.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public User getById(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAllByName(String name) {
        return userRepository.findAllByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public void deposit(User user, double amount) {
        UserAccount userAccount = getUserAccount(user);
        userAccount.setAmount(userAccount.getAmount() + amount);
        userAccountRepository.save(userAccount);
    }

    @Override
    public void withdraw(User user, double amount) {
        UserAccount userAccount = null;
        try {
            userAccount = userAccountRepository.findByUser(user);
        } catch (EmptyResultDataAccessException ex) {
            throw new InsufficientFundsException(user);
        }
        if (userAccount.getAmount() < amount) {
            throw new InsufficientFundsException(user);
        }

        userAccount.setAmount(userAccount.getAmount() - amount);
        userAccountRepository.save(userAccount);
    }

    @Override
    public UserAccount getUserAccount(User user) {
        UserAccount userAccount = null;
        try {
            userAccount = userAccountRepository.findByUser(user);
        } catch (EmptyResultDataAccessException ex) {
            userAccount = new UserAccount();
            userAccount.setUser(user);
        }
        return userAccount;
    }
}
