package ua.pp.kaeltas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.statistics.StatisticsRepository;
import ua.pp.kaeltas.service.StatisticsService;

@Service
@Transactional
public class StatisticsServiceImpl implements StatisticsService {

    @Autowired
    private StatisticsRepository statisticsRepository;

    @Override
    public void incrementEventRequestsCount(Event event) {
        int eventRequestsCount = statisticsRepository.getEventRequestsCount(event);
        statisticsRepository.saveEventRequestsCount(event, ++eventRequestsCount);
    }

    @Override
    @Transactional(readOnly = true)
    public int getEventRequestsCount(Event event) {
        return statisticsRepository.getEventRequestsCount(event);
    }

    @Override
    public void incrementEventTicketPriceRequestsCount(Event event) {
        int eventTicketPriceRequestsCount = statisticsRepository.getEventTicketPriceRequestsCount(event);
        statisticsRepository.saveEventTicketPriceRequestsCount(event, ++eventTicketPriceRequestsCount);
    }

    @Override
    @Transactional(readOnly = true)
    public int getEventTicketPriceRequestsCount(Event event) {
        return statisticsRepository.getEventTicketPriceRequestsCount(event);
    }

    @Override
    public void incrementBookTicketRequestsCount(Event event) {
        int bookTicketRequestsCount = statisticsRepository.getBookTicketRequestsCount(event);
        statisticsRepository.saveBookTicketRequestsCount(event, ++bookTicketRequestsCount);
    }

    @Override
    @Transactional(readOnly = true)
    public int getBookTicketRequestsCount(Event event) {
        return statisticsRepository.getBookTicketRequestsCount(event);
    }

    @Override
    public void incrementTotalDiscountRequestsCount() {
        int totalDiscountRequestsCount = statisticsRepository.getTotalDiscountRequestsCount();
        statisticsRepository.saveTotalDiscountRequestsCount(++totalDiscountRequestsCount);
    }

    @Override
    @Transactional(readOnly = true)
    public int getTotalDiscountRequestsCount() {
        return statisticsRepository.getTotalDiscountRequestsCount();
    }

    @Override
    public void incrementUserDiscountRequestsCount(User user) {
        if (null != user) {
            int userDiscountRequestsCount = statisticsRepository.getUserDiscountRequestsCount(user);
            statisticsRepository.saveUserDiscountRequestsCount(user, ++userDiscountRequestsCount);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public int getUserDiscountRequestsCount(User user) {
        if (null == user) {
            return 0;
        }
        return statisticsRepository.getUserDiscountRequestsCount(user);
    }


}
