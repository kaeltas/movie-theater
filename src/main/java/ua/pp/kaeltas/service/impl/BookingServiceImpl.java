package ua.pp.kaeltas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.dto.rest.TicketForBookingRequestDto;
import ua.pp.kaeltas.domain.dto.rest.TicketResponseDto;
import ua.pp.kaeltas.domain.dto.soap.request.BookTicketRequest;
import ua.pp.kaeltas.exception.TicketAlreadyBookedException;
import ua.pp.kaeltas.repository.ticket.TicketRepository;
import ua.pp.kaeltas.service.BookingService;
import ua.pp.kaeltas.service.DiscountService;
import ua.pp.kaeltas.service.EventService;
import ua.pp.kaeltas.service.TicketPriceCalculator;
import ua.pp.kaeltas.service.UserService;

@Slf4j
@Service
@Transactional
public class BookingServiceImpl implements BookingService {

    @Autowired
    private EventService eventService;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private DiscountService discountService;

    @Autowired
    private TicketPriceCalculator ticketPriceCalculator;

    @Autowired
    private UserService userService;

    @Override
    @Transactional(readOnly = true)
    public double getTicketPrice(Event event, LocalDateTime dateTime, Set<Integer> seats, User user) {

        PlannedEvent plannedEvent = eventService.getPlannedEvent(event, dateTime);

        List<Ticket> tickets = seats.stream()
                .map(seat -> createTicket(plannedEvent, seat))
                .collect(Collectors.toList());

        double ticketsSumPrice = tickets.stream()
                .mapToDouble(Ticket::getPrice)
                .sum();

        log.info("{} tickets, base sum price {}", tickets.size(), ticketsSumPrice);

        double ticketsSumDiscount = discountService.getDiscount(user, tickets);

        log.info("{} tickets, sum discount {}", tickets.size(), ticketsSumDiscount);

        return ticketsSumPrice - ticketsSumDiscount;

    }

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime date) {
        return ticketRepository.findAllByEvent(event, date);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> getPurchasedTickets(User user) {
        return ticketRepository.findAllByUser(user);
    }

    @Override
    public void bookTicket(User user, Ticket ticket) {
        if (ticketRepository.isPurchased(ticket.getPlannedEvent(), ticket.getSeat())) {
            throw new TicketAlreadyBookedException(ticket);
        }
        ticket.setUser(user);
        ticketRepository.save(ticket);
        userService.withdraw(user, ticket.getPrice());
    }

    @Lookup
    @Override
    public Ticket getNewTicket() {
        throw new RuntimeException("getNewTicket() method should have been overridden by Spring Lookup-method");
    }

    @Override
    public Ticket createTicket(PlannedEvent plannedEvent, int seat) {
        Ticket ticket = getNewTicket();
        ticket.setPlannedEvent(plannedEvent);
        ticket.setSeat(seat);
        ticket.setPrice(ticketPriceCalculator.calcTicketPrice(plannedEvent, seat));

        return ticket;
    }

    @Override
    public List<TicketResponseDto> getAllTickets() {
        return ticketRepository.findAll().stream()
                .map(TicketResponseDto::new)
                .collect(Collectors.toList());
    }

    @Override
    public TicketResponseDto getTicket(int id) {
        return new TicketResponseDto(ticketRepository.findById(id));
    }

    @Override
    public void bookTicket(TicketForBookingRequestDto ticketForBookingRequestDto) {
        PlannedEvent plannedEvent = eventService.getPlannedEventById(ticketForBookingRequestDto.getPlannedEventId());
        Ticket ticket = createTicket(plannedEvent, ticketForBookingRequestDto.getSeat());
        User user = userService.getById(ticketForBookingRequestDto.getUserId());
        bookTicket(user, ticket);
    }

    @Override
    public Ticket bookTicket(BookTicketRequest request) {
        PlannedEvent plannedEvent = eventService.getPlannedEventById(request.getPlannedEventId());
        Ticket ticket = createTicket(plannedEvent, request.getSeat());
        User user = userService.getById(request.getUserId());
        bookTicket(user, ticket);

        return ticket;
    }

}
