package ua.pp.kaeltas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.discount.DiscountStrategy;
import ua.pp.kaeltas.service.DiscountService;

import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService {
    @Autowired
    private List<DiscountStrategy> discountStrategies;

    @Override
    public double getDiscount(User user, List<Ticket> tickets) {
        float totalDiscount = 0;
        for (DiscountStrategy ds : discountStrategies) {
            totalDiscount += ds.getDiscount(user, tickets);
        }

        return totalDiscount;
    }
}
