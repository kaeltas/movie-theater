package ua.pp.kaeltas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.exception.EventHasAlreadyBeenPlannedException;
import ua.pp.kaeltas.exception.SuchAuditoriumAlreadyAssignedException;
import ua.pp.kaeltas.repository.event.EventRepository;
import ua.pp.kaeltas.repository.event.PlannedEventRepository;
import ua.pp.kaeltas.service.EventService;

@Service
@Transactional
public class EventServiceImpl implements EventService {
    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private PlannedEventRepository plannedEventRepository;


    @Override
    public void create(Event event) {
        eventRepository.save(event);
    }

    @Override
    public void remove(String name) {
        if (plannedEventRepository.contains(name)) {
            throw new EventHasAlreadyBeenPlannedException("Can't remove event, because it has been planned: name=" + name);
        }
        eventRepository.remove(name);
    }

    @Override
    @Transactional(readOnly = true)
    public Event getByName(String name) {
        return eventRepository.findByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public Event getById(Integer eventId) {
        return eventRepository.findById(eventId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Event> getAll() {
        return eventRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlannedEvent> getForDateRange(LocalDateTime from, LocalDateTime to) {
        return plannedEventRepository.findForDateRange(from, to);
    }

    @Override
    public void assignAuditorium(Event event, Auditorium auditorium, LocalDateTime date) {
        List<PlannedEvent> allByDate = plannedEventRepository.findAllByDate(date);
        if (allByDate.stream().anyMatch(p -> p.getAuditorium().equals(auditorium))) {
            throw new SuchAuditoriumAlreadyAssignedException(auditorium, date);
        }
        plannedEventRepository.assignAuditorium(event, auditorium, date);
    }

    @Override
    @Transactional(readOnly = true)
    public PlannedEvent getPlannedEvent(Event event, LocalDateTime dateTime) {
        return plannedEventRepository.findByEventAndDate(event, dateTime);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlannedEvent> getPlannedEvents(Event event) {
        return plannedEventRepository.findAllByEvent(event);
    }

    @Override
    @Transactional(readOnly = true)
    public PlannedEvent getPlannedEventById(Integer id) {
        return plannedEventRepository.findById(id);
    }
}
