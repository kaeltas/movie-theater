package ua.pp.kaeltas.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.repository.auditorium.AuditoriumRepository;
import ua.pp.kaeltas.service.AuditoriumService;

import java.util.List;
import java.util.Set;

@Service
public class AuditoriumServiceImpl implements AuditoriumService {
    @Autowired
    private AuditoriumRepository auditoriumRepository;

    @Override
    public List<Auditorium> getAuditoriums() {
        return auditoriumRepository.getAuditoriums();
    }

    @Override
    public Auditorium getAuditoriumByName(String auditoriumName) {
        return auditoriumRepository.findAuditoriumByName(auditoriumName);
    }

    @Override
    public int getSeatsNumber(String auditoriumName) {
        return auditoriumRepository.getSeatsNumber(auditoriumName);
    }

    @Override
    public Set<Integer> getVipSeats(String auditoriumName) {
        return auditoriumRepository.getVipSeats(auditoriumName);
    }
}
