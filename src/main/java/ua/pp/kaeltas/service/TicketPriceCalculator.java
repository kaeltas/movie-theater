package ua.pp.kaeltas.service;

import ua.pp.kaeltas.domain.PlannedEvent;

@FunctionalInterface
public interface TicketPriceCalculator {
    double calcTicketPrice(PlannedEvent plannedEvent, int seat);
}
