package ua.pp.kaeltas.service;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.User;

public interface StatisticsService {

    void incrementEventRequestsCount(Event event);
    int getEventRequestsCount(Event event);

    void incrementEventTicketPriceRequestsCount(Event event);
    int getEventTicketPriceRequestsCount(Event event);

    void incrementBookTicketRequestsCount(Event event);
    int getBookTicketRequestsCount(Event event);

    void incrementTotalDiscountRequestsCount();
    int getTotalDiscountRequestsCount();

    void incrementUserDiscountRequestsCount(User user);
    int getUserDiscountRequestsCount(User user);
}
