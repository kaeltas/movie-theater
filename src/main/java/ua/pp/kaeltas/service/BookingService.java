package ua.pp.kaeltas.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.PlannedEvent;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.dto.rest.TicketForBookingRequestDto;
import ua.pp.kaeltas.domain.dto.rest.TicketResponseDto;
import ua.pp.kaeltas.domain.dto.soap.request.BookTicketRequest;

public interface BookingService {

    /**
     * Returns sum price for tickets for specified event on specific date and time for specified seats.
     *
     */
    double getTicketPrice(Event event, LocalDateTime date, Set<Integer> seats, User user);

    /**
     * Get all purchased tickets for event for specific date and auditorium
     *
     */
    List<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime date);

    /**
     * Get all purchased tickets by User
     *
     */
    List<Ticket> getPurchasedTickets(User user);

    /**
     * User could  be registered or not.
     * If user is registered, then booking information is stored for that user.
     * Purchased tickets for particular event should be stored
     */
    void bookTicket(User user, Ticket ticket);

    Ticket getNewTicket();

    Ticket createTicket(PlannedEvent plannedEvent, int seat);

    List<TicketResponseDto> getAllTickets();

    TicketResponseDto getTicket(int id);

    void bookTicket(TicketForBookingRequestDto ticketForBookingRequestDto);

    Ticket bookTicket(BookTicketRequest request);
}
