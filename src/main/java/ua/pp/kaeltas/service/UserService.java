package ua.pp.kaeltas.service;

import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.domain.UserAccount;

import java.util.List;

public interface UserService {

    void register(User user);
    void remove(Integer id);
    void remove(User user);
    User getById(Integer id);
    User getByEmail(String email);
    List<User> getAllByName(String name);
    List<User> getAll();

    void deposit(User user, double amount);
    void withdraw(User user, double amount);
    UserAccount getUserAccount(User user);

}
