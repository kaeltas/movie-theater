package ua.pp.kaeltas.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import ua.pp.kaeltas.domain.Auditorium;

@Slf4j
@Configuration
public class AuditoriumConfig {

    @Bean
    public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurerVenus() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocation(new ClassPathResource("auditorium-venus.properties"));
        propertyPlaceholderConfigurer.setPlaceholderPrefix("${venus.");

        return propertyPlaceholderConfigurer;
    }

    @Bean
    public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurerMercury() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        propertyPlaceholderConfigurer.setLocation(new ClassPathResource("auditorium-mercury.properties"));
        propertyPlaceholderConfigurer.setPlaceholderPrefix("${mercury.");

        return propertyPlaceholderConfigurer;
    }

    @Bean
    public Auditorium mercuryAuditorium(@Value("${mercury.name}") String name,
                                        @Value("${mercury.seats.total}") int totalSeats,
                                        @Value("${mercury.seats.vip}") String vipSeats) {
        return createAuditorium(name, totalSeats, vipSeats);
    }

    @Bean
    public Auditorium venusAuditorium(@Value("${venus.name}") String name,
                                      @Value("${venus.seats.total}") int totalSeats,
                                      @Value("${venus.seats.vip}") String vipSeats) {
        return createAuditorium(name, totalSeats, vipSeats);
    }

    private Auditorium createAuditorium(String name, int totalSeats, String vipSeats) {
        log.debug("Is about to create auditorium name={}, totalSeats={}, vipsetats={}", name, totalSeats, vipSeats);
        Auditorium auditorium = new Auditorium();
        auditorium.setName(name);
        auditorium.setTotalNumberOfSeats(totalSeats);

        Set<Integer> vipSeatsSet = Arrays.stream(vipSeats.split(","))
                .filter(s -> !s.isEmpty())
                .map(Integer::parseInt)
                .collect(Collectors.toSet());

        log.debug("vipSeatsSet = {} ", vipSeatsSet);

        auditorium.setVipSeats(vipSeatsSet);

        return auditorium;
    }

}
