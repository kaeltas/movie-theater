package ua.pp.kaeltas.config;

import org.springframework.context.annotation.Profile;

@Profile({"InMemory"})
public @interface ProfileInMemory {
}
