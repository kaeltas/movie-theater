package ua.pp.kaeltas.config;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;

import ua.pp.kaeltas.domain.Ticket;

@Configuration
@ComponentScan("ua.pp.kaeltas")
@EnableAspectJAutoProxy
public class ContextConfig {

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public Ticket ticket() {
        return new Ticket();
    }

}
