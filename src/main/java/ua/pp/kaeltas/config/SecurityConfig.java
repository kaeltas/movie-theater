package ua.pp.kaeltas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import ua.pp.kaeltas.domain.Role;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String USERS_BY_USERNAME_QUERY = "SELECT name,password_hash,1 from User WHERE name = ?";
    public static final String AUTHORITIES_BY_USERNAME_QUERY = "SELECT username,role from Role WHERE username = ?";
    public static final String ROLE_PREFIX = "ROLE_";

    @Autowired
    private DriverManagerDataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/logout", "/login", "/login-error", "/resources/**").permitAll()
                .antMatchers("/user/**", "/upload/**").hasRole(Role.BOOKING_MANAGER)
                .antMatchers("/event/**").hasAnyRole(Role.REGISTERED_USER, Role.BOOKING_MANAGER)
                .antMatchers("/rest").hasRole(Role.BOOKING_MANAGER)
                .antMatchers( "/ws/**").permitAll()
                .anyRequest().authenticated()
                .and().csrf().ignoringAntMatchers("/rest/**", "/ws/**")
                .and().httpBasic()
                .and().formLogin()
                        .loginPage("/login").usernameParameter("username").passwordParameter("password")
                        .failureUrl("/login-error")
                .and().logout().logoutSuccessUrl("/")
                .and().exceptionHandling().accessDeniedPage("/access-denied")
                .and().rememberMe().tokenRepository(persistentTokenRepository()).rememberMeParameter("remember-me");
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        tokenRepositoryImpl.setDataSource(dataSource);
        return tokenRepositoryImpl;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .passwordEncoder(bCryptPasswordEncoder())
                .dataSource(dataSource)
                .rolePrefix(ROLE_PREFIX)
                .usersByUsernameQuery(USERS_BY_USERNAME_QUERY)
                .authoritiesByUsernameQuery(AUTHORITIES_BY_USERNAME_QUERY);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
