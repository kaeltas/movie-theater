package ua.pp.kaeltas.config;

import org.springframework.context.annotation.Profile;

@Profile({"Jdbc", "default"})
public @interface ProfileJdbc {
}
