package ua.pp.kaeltas.exception;

import ua.pp.kaeltas.domain.Auditorium;

import java.time.LocalDateTime;

public class SuchAuditoriumAlreadyAssignedException extends RuntimeException {

    public SuchAuditoriumAlreadyAssignedException(Auditorium auditorium, LocalDateTime dateTime) {
        super("Such auditorium already assigned: " + auditorium + " for " + dateTime);
    }

}
