package ua.pp.kaeltas.exception;

public class EventHasAlreadyBeenPlannedException extends RuntimeException {

    public EventHasAlreadyBeenPlannedException(String message) {
        super(message);
    }

}
