package ua.pp.kaeltas.exception;

import ua.pp.kaeltas.domain.Event;

public class SuchEventAlreadyExistsException extends RuntimeException {

    public SuchEventAlreadyExistsException(Event event) {
        super("Event already exists: " + event);
    }
}
