package ua.pp.kaeltas.exception;

public class NoSuchAuditoriumException extends RuntimeException {

    public NoSuchAuditoriumException(String auditoriumName) {
        super("No such auditorium: name=" + auditoriumName);
    }

    public NoSuchAuditoriumException(int id) {
        super("No such auditorium: id=" + id);
    }
}
