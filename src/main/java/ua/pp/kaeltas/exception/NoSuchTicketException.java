package ua.pp.kaeltas.exception;

public class NoSuchTicketException extends RuntimeException {

    public NoSuchTicketException(int id) {
        super("No such ticket: ticketId=" + id);
    }
}
