package ua.pp.kaeltas.exception;

import ua.pp.kaeltas.domain.Event;

import java.time.LocalDateTime;

public class NoSuchPlannedEventException extends RuntimeException {

    public NoSuchPlannedEventException(Event event, LocalDateTime dateTime) {
        super("No such planned event: " + event + ", dateTime=" + dateTime);
    }

    public NoSuchPlannedEventException(int id) {
        super("No such planned event with id = " + id);
    }
}
