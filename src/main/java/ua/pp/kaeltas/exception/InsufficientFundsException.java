package ua.pp.kaeltas.exception;

import ua.pp.kaeltas.domain.User;

public class InsufficientFundsException extends RuntimeException {

    public InsufficientFundsException(User user) {
        super("User [id=" + user.getId() + ", name=" + user.getName() + "] doesn't have enough money on account!");
    }
}
