package ua.pp.kaeltas.exception;

import ua.pp.kaeltas.domain.Ticket;

public class TicketAlreadyBookedException extends RuntimeException {

    public TicketAlreadyBookedException(Ticket ticket) {
        super("Ticket already has been booked: " + ticket);
    }
}
