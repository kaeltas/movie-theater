package ua.pp.kaeltas.exception;

public class NoSuchEventException extends RuntimeException {

    public NoSuchEventException(String eventName) {
        super("No such event: eventName=" + eventName);
    }

    public NoSuchEventException(int id) {
        super("No such event: eventId=" + id);
    }
}
