CREATE TABLE user (
  id            INT PRIMARY KEY AUTO_INCREMENT,
  name          VARCHAR(255),
  email         VARCHAR(255),
  birthday      DATETIME,
  password_hash VARCHAR(255)
);

ALTER TABLE user
ADD CONSTRAINT uq_email UNIQUE (email);

CREATE TABLE user_account (
  id      INT PRIMARY KEY AUTO_INCREMENT,
  user_id INT,
  amount  DOUBLE,

  FOREIGN KEY (user_id)
  REFERENCES user (id)
);

CREATE TABLE role (
  id       INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(255),
  role     VARCHAR(255)
);

CREATE TABLE persistent_logins (
  username  VARCHAR(64) NOT NULL,
  series    VARCHAR(64) PRIMARY KEY,
  token     VARCHAR(64) NOT NULL,
  last_used TIMESTAMP   NOT NULL
);

CREATE TABLE event (
  id         INT PRIMARY KEY AUTO_INCREMENT,
  name       VARCHAR(255),
  rating     VARCHAR(255),
  base_price DOUBLE
);

ALTER TABLE event
ADD CONSTRAINT uq_event UNIQUE (name);


CREATE TABLE auditorium (
  id                    INT PRIMARY KEY AUTO_INCREMENT,
  name                  VARCHAR(255),
  total_number_of_seats INT
);

CREATE TABLE auditorium_vip_seats (
  id            INT PRIMARY KEY AUTO_INCREMENT,
  auditorium_id INT,
  vip_seat      INT
);

ALTER TABLE auditorium_vip_seats
ADD CONSTRAINT uq_auditorium_vip_seats UNIQUE (auditorium_id, vip_seat);

CREATE TABLE planned_event (
  id            INT PRIMARY KEY AUTO_INCREMENT,
  event_id      INT,
  auditorium_id INT,
  datetime      DATETIME
);

CREATE TABLE ticket (
  id               INT PRIMARY KEY AUTO_INCREMENT,
  user_id          INT,
  planned_event_id INT,
  seat             INT,
  price            DOUBLE
);


CREATE TABLE event_statistics (
  id             INT PRIMARY KEY AUTO_INCREMENT,
  event_id       INT,
  statistic_type INT,
  value          INT
);

CREATE TABLE user_statistics (
  id      INT PRIMARY KEY AUTO_INCREMENT,
  user_id INT,
  value   INT
);


