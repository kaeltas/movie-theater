package ua.pp.kaeltas.repository.user;

import org.junit.Before;
import org.junit.Test;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.exception.NoSuchUserException;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;

public class InMemoryUserRepositoryTest {

    private UserRepository userRepository;

    @Before
    public void setUp() {
        userRepository = new InMemoryUserRepository();;
    }

    @Test
    public void saveShouldGenerateIdForUser() throws Exception {
        User expected = new User("John", "john@email.com", LocalDate.of(1990, Month.FEBRUARY, 1));

        userRepository.save(expected);

        assertThat(expected.getId(), is(notNullValue()));
    }

    @Test
    public void saveShouldStoreUser() throws Exception {
        User expected = new User("John", "john@email.com", LocalDate.of(1990, Month.FEBRUARY, 1));

        userRepository.save(expected);

        User actual = userRepository.findById(expected.getId());

        assertThat(expected, is(equalTo(actual)));
    }

    @Test(expected = NoSuchUserException.class)
    public void removeShouldDeleteUserFromRepository() throws Exception {
        User expected = new User("John", "john@email.com", LocalDate.of(1990, Month.FEBRUARY, 1));
        userRepository.save(expected);

        userRepository.remove(expected.getId());

        userRepository.findById(expected.getId());
    }

    @Test(expected = NoSuchUserException.class)
    public void findByIdShouldThrowExceptionIfNoSuchUserFound() throws Exception {
        userRepository.findById(0);
    }

    @Test(expected = NoSuchUserException.class)
    public void findByEmailShouldThrowExceptionIfNoneFound() throws Exception {
        User expected = new User("John", "john@email.com", LocalDate.of(1990, Month.FEBRUARY, 1));

        userRepository.findByEmail(expected.getEmail());
    }

    @Test
    public void findAllByNameInRepositoryWith2UsersShouldFind2Users() throws Exception {
        User user1 = new User("John", "john@email.com", LocalDate.of(1990, Month.FEBRUARY, 1));
        User user2 = new User("John", "john2@email.com", LocalDate.of(1990, Month.FEBRUARY, 1));

        userRepository.save(user1);
        userRepository.save(user2);

        List<User> actualUsers = userRepository.findAllByName("John");

        assertThat(actualUsers.size(), is(2));
        assertThat(actualUsers, hasItems(user1, user2));
    }

    @Test
    public void findAllByNameInEmptyRepositoryShouldReturnEmptyList() throws Exception {
        List<User> actualUsers = userRepository.findAllByName("John");

        assertThat(actualUsers.size(), is(0));
    }
}