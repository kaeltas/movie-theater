package ua.pp.kaeltas.repository.event;

import org.junit.Before;
import org.junit.Test;
import ua.pp.kaeltas.domain.Auditorium;
import ua.pp.kaeltas.domain.Event;
import ua.pp.kaeltas.domain.EventRating;
import ua.pp.kaeltas.domain.PlannedEvent;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class InMemoryPlannedEventRepositoryTest {

    private PlannedEventRepository plannedEventRepository;

    @Before
    public void setUp() {
        plannedEventRepository = new InMemoryPlannedEventRepository();
    }

    @Test
    public void getForDateRangeShouldReturnPlannedEventsInSelectedRange() throws Exception {
        Event event = new Event("CoolMovie", 100, EventRating.MID);
        LocalDateTime dateTime1 = LocalDateTime.of(2016, Month.FEBRUARY, 7, 13, 00);
        LocalDateTime dateTime2 = LocalDateTime.of(2016, Month.FEBRUARY, 7, 15, 00);
        Auditorium auditorium = new Auditorium();
        auditorium.setName("TestAuditorium");

        plannedEventRepository.assignAuditorium(event, auditorium, dateTime1);
        plannedEventRepository.assignAuditorium(event, auditorium, dateTime2);

        LocalDateTime from = LocalDateTime.of(2016, Month.FEBRUARY, 7, 13, 00);
        LocalDateTime to = LocalDateTime.of(2016, Month.FEBRUARY, 7, 15, 00);
        List<PlannedEvent> actualPlannedEvents = plannedEventRepository.findForDateRange(from, to);

        assertThat(actualPlannedEvents.size(), is(equalTo(2)));
    }

}