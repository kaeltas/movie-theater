package ua.pp.kaeltas.repository.auditorium;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ua.pp.kaeltas.config.ContextConfig;
import ua.pp.kaeltas.exception.NoSuchAuditoriumException;

import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ContextConfig.class)
@ActiveProfiles("InMemory")
public class InMemoryAuditoriumRepositoryTest {

    @Autowired
    AuditoriumRepository auditoriumRepository;

    @Test
    public void getAuditoriumsListSizeShouldReturnPredefinedAuditoriumsCount() throws Exception {
        assertThat(auditoriumRepository.getAuditoriums().size(), is(equalTo(2)));
    }

    @Test
    public void getSeatsNumberForMercuryAuditoriumShouldReturnPredefinedSeatsCount() throws Exception {
        String auditoriumName = "Mercury";
        int actualTotalNumberOfSeats = 40;

        int expected = auditoriumRepository.getSeatsNumber(auditoriumName);
        assertThat(expected, is(equalTo(actualTotalNumberOfSeats)));
    }

    @Test(expected = NoSuchAuditoriumException.class)
    public void getSeatsNumberShouldThrowExceptionIfAuditoriumNotFount() throws Exception {
        String auditoriumName = "AbsentAuditoriumName";

        auditoriumRepository.getSeatsNumber(auditoriumName);
    }

    @Test
    public void getVipSeatsForMercuryAuditoriumShouldReturnPredefinedVipSeatsCount() throws Exception {
        String auditoriumName = "Mercury";
        Integer[] actualVipSeats = new Integer[] {1, 2, 3, 4, 5, 6, 7, 8};

        Set<Integer> expectedVipSeats = auditoriumRepository.getVipSeats(auditoriumName);

        assertThat(expectedVipSeats.size(), is(equalTo(actualVipSeats.length)));
        assertThat(expectedVipSeats, hasItems(actualVipSeats));
    }

    @Test(expected = NoSuchAuditoriumException.class)
    public void getVipSeatsShouldThrowExceptionIfAuditoriumNotFount() throws Exception {
        String auditoriumName = "AbsentAuditoriumName";

        auditoriumRepository.getVipSeats(auditoriumName);
    }
}