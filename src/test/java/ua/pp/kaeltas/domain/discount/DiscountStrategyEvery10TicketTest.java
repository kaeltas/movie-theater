package ua.pp.kaeltas.domain.discount;

import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ua.pp.kaeltas.domain.Ticket;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.ticket.TicketRepository;
import ua.pp.kaeltas.service.TicketPriceCalculator;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class DiscountStrategyEvery10TicketTest {
    private static double EPS = 0.001;

    @InjectMocks
    private DiscountStrategy discountStrategy = new DiscountStrategyEvery10Ticket();

    @Mock
    private TicketRepository ticketRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @DataPoints
    public static Object[][] testData() {
        return new Object[][] {
                {0, 0, 100.0, 0.0},
                {0, 9, 100.0, 0.0},
                {9, 0, 100.0, 0.0},
                {8, 1, 100.0, 0.0},
                {0, 10, 100.0, 50.0},
                {9, 1, 100.0, 50.0},
                {10, 1, 100.0, 0.0},
                {10, 0, 100.0, 0.0},
                {0, 20, 100.0, 100.0},
                {3, 7, 100.0, 50.0},
        };
    }

    @Theory
    public void getDiscountShouldReturnCorrectDiscount(final Object...params) throws Exception {
        int numberOfPurchasedTickets = (int)params[0];
        int numberOfTicketsToBuy = (int)params[1];
        double singleTicketPrice = (double)params[2];
        double expectedDiscount = (double)params[3];

        User user = mock(User.class);

        Ticket ticket = mock(Ticket.class);
        when(ticket.getPrice()).thenReturn(singleTicketPrice);
        List<Ticket> ticketsToBuy = Collections.nCopies(numberOfTicketsToBuy, ticket);

        List<Ticket> alreadyPurchasedTickets = mock(List.class);
        when(alreadyPurchasedTickets.size()).thenReturn(numberOfPurchasedTickets);
        when(ticketRepository.findAllByUser(any())).thenReturn(alreadyPurchasedTickets);

        double actualDiscount = discountStrategy.getDiscount(user, ticketsToBuy);

        assertThat(actualDiscount, is(equalTo(expectedDiscount)));
    }

}