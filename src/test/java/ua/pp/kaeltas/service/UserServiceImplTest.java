package ua.pp.kaeltas.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ua.pp.kaeltas.domain.User;
import ua.pp.kaeltas.repository.user.UserRepository;
import ua.pp.kaeltas.service.impl.UserServiceImpl;

import java.time.LocalDate;
import java.time.Month;

import static org.mockito.Mockito.*;

public class UserServiceImplTest {

    @InjectMocks
    private UserService userService = new UserServiceImpl();

    @Mock
    private UserRepository userRepositoryMock;

    @Before
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void registerShouldTriggerRepositorySaveOnlyOnce() throws Exception {
        User user = new User("John", "john@email.com", LocalDate.of(1990, Month.FEBRUARY, 1));
        userService.register(user);

        verify(userRepositoryMock, times(1)).save(user);
        verifyNoMoreInteractions(userRepositoryMock);
    }

    @Test
    public void removeByIdShouldTriggerRepositoryRemoveByIdOnlyOnce() throws Exception {
        Integer userId = 0;
        userService.remove(userId);

        verify(userRepositoryMock, times(1)).remove(userId);
        verifyNoMoreInteractions(userRepositoryMock);
    }

    @Test
    public void removeByUserShouldTriggerRepositoryRemoveByIdOnlyOnce() throws Exception {
        User user = mock(User.class);
        userService.remove(user);

        verify(userRepositoryMock, times(1)).remove(user.getId());
        verifyNoMoreInteractions(userRepositoryMock);
    }

    @Test
    public void getByIdShouldOnlyTriggerFindByIdOnce() throws Exception {
        Integer userId = 0;
        userService.getById(userId);

        verify(userRepositoryMock, times(1)).findById(userId);
        verifyNoMoreInteractions(userRepositoryMock);
    }

    @Test
    public void getByEmailShouldOnlyTriggerFindByEmailOnce() throws Exception {
        String userEmail = "user@email.com";
        userService.getByEmail(userEmail);

        verify(userRepositoryMock, times(1)).findByEmail(userEmail);
        verifyNoMoreInteractions(userRepositoryMock);
    }

    @Test
    public void getAllByNameShouldOnlyTriggerFindAllByNameOnce() throws Exception {
        String userName = "John";
        userService.getAllByName(userName);

        verify(userRepositoryMock, times(1)).findAllByName(userName);
        verifyNoMoreInteractions(userRepositoryMock);
    }
}