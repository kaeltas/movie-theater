### Run project
    ./gradlew tomcatRun

#### Files for importing users and events:
[http://localhost:8080/movie-theater/upload]()

    users.json
    events.json


#### Credentials
    login: user
    password: user

    login: admin
    password: admin
